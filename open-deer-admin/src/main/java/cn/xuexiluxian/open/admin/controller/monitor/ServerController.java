package cn.xuexiluxian.open.admin.controller.monitor;

import cn.xuexiluxian.open.common.annotation.Log;
import cn.xuexiluxian.open.common.enums.BusinessType;
import cn.xuexiluxian.open.common.response.ResponseResult;
import cn.xuexiluxian.open.framework.entity.Server;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务监控
 */
@RestController
@RequestMapping("monitor/server")
public class ServerController {

    @GetMapping()
    @Log(title = "服务监控", businessType = BusinessType.SELECT)
    @PreAuthorize("@permission.hasPermission('monitor:server:list')")
    public ResponseResult getInfo() throws Exception {
        Server server = new Server();
        server.copyTo();
        return ResponseResult.ok(server);
    }
}
