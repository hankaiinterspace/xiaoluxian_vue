package cn.xuexiluxian.open.admin.controller.logger;

import cn.hutool.core.util.StrUtil;
import cn.xuexiluxian.open.common.annotation.Log;
import cn.xuexiluxian.open.common.annotation.RepeatSubmit;
import cn.xuexiluxian.open.common.enums.BusinessType;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.common.enums.ResultStatusCodeEnum;
import cn.xuexiluxian.open.common.response.ResponseResult;
import cn.xuexiluxian.open.system.model.entity.SystemLoginLog;
import cn.xuexiluxian.open.system.model.entity.SystemOperLog;
import cn.xuexiluxian.open.system.model.req.LoginLogREQ;
import cn.xuexiluxian.open.system.model.req.OperateLogREQ;
import cn.xuexiluxian.open.system.service.ISystemLoginLogService;
import cn.xuexiluxian.open.system.service.ISystemOperLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 操作日志
 */
@Slf4j
@RestController
@RequestMapping("logger/operate")
public class OperateLoggerController {
    @Autowired
    private ISystemOperLogService systemOperLogService;

    @GetMapping(value = "/page")
    @Log(title = "操作日志", businessType = BusinessType.SELECT)
    @PreAuthorize("@permission.hasPermission('logger:operate:page')")
    public ResponseResult page(Page page, OperateLogREQ operateLogREQ){
        Page<SystemOperLog> pageList = systemOperLogService.pageList(page, operateLogREQ);
        return ResponseResult.ok(pageList);
    }

    @GetMapping(value = "get/{id}")
    @Log(title = "操作日志", businessType = BusinessType.SELECT)
    @PreAuthorize("@permission.hasPermission('logger:operate:get')")
    public ResponseResult get(@PathVariable String id) {
        if(StrUtil.isBlank(id)){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        SystemOperLog systemOperLog = systemOperLogService.getById(id);
        if(systemOperLog != null){
            return ResponseResult.ok(systemOperLog);
        }
        return ResponseResult.error(ResultStatusCodeEnum.DATA_NO_EXIST);
    }

    @RepeatSubmit
    @PostMapping(value = "delete")
    @Log(title = "操作日志", businessType = BusinessType.DELETE)
    @PreAuthorize("@permission.hasPermission('logger:operate:delete')")
    public ResponseResult delete(@RequestBody List<String> ids){
        boolean b = systemOperLogService.removeByIds(ids);
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }

    @RepeatSubmit
    @GetMapping(value = "clean")
    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @PreAuthorize("@permission.hasPermission('logger:operate:clean')")
    public ResponseResult clean(){
        boolean b = systemOperLogService.clean();
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }
}
