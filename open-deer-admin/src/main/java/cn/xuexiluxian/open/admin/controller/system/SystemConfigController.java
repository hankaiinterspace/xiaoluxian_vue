package cn.xuexiluxian.open.admin.controller.system;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.xuexiluxian.open.common.annotation.Log;
import cn.xuexiluxian.open.common.annotation.RepeatSubmit;
import cn.xuexiluxian.open.common.enums.BusinessType;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.common.enums.ResultStatusCodeEnum;
import cn.xuexiluxian.open.common.response.ResponseResult;
import cn.xuexiluxian.open.system.model.entity.SystemConfig;
import cn.xuexiluxian.open.system.model.req.SystemConfigREQ;
import cn.xuexiluxian.open.system.service.ISystemConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 参数配置
 */
@Slf4j
@RestController
@RequestMapping("system/config")
public class SystemConfigController {
    @Autowired
    private ISystemConfigService systemConfigService;

    /**
     * 分页查询参数配置
     * @param page
     * @param params
     * @return
     */
    @GetMapping(value = "page")
    @Log(title = "参数配置", businessType = BusinessType.SELECT)
    @PreAuthorize("@permission.hasPermission('system:config:page')")
    public ResponseResult page(Page page, SystemConfigREQ params) {
        Page<SystemConfig> pageList = systemConfigService.pageList(page, params);
        return ResponseResult.ok(pageList);
    }

    @GetMapping("all")
    @Log(title = "参数配置", businessType = BusinessType.SELECT)
    @PreAuthorize("@permission.hasPermission('system:config:select')")
    public ResponseResult all(){
        List<SystemConfig> list = systemConfigService.list();
        return ResponseResult.ok(list);
    }

    /**
     * 删除参数配置
     * @param id
     * @return
     */
    @RepeatSubmit
    @GetMapping("delete/{id}")
    @Log(title = "参数配置", businessType = BusinessType.DELETE)
    @PreAuthorize("@permission.hasPermission('system:config:delete')")
    public ResponseResult delete(@PathVariable String id){
        if(StrUtil.isBlank(id)){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        SystemConfig systemConfig = systemConfigService.getById(id);
        if(systemConfig == null){
            return ResponseResult.error(ResultStatusCodeEnum.DATA_NO_EXIST);
        }
        if(systemConfig.getType() == 1){
            return ResponseResult.error(ResultStatusCodeEnum.BUILT_IN_DATA_NODELETE);
        }
        boolean b = systemConfigService.removeById(id);
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }

    /**
     * 参数配置详情
     * @param key
     * @return
     */
    @GetMapping("get/{key}")
    @Log(title = "参数配置", businessType = BusinessType.SELECT)
    @PreAuthorize("@permission.hasPermission('system:config:get')")
    public ResponseResult get(@PathVariable String key){
        if(StrUtil.isBlank(key)){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        SystemConfig systemConfig = systemConfigService.selectOneByKey(key);
        if(systemConfig != null){
            return ResponseResult.ok(systemConfig);
        }
        return ResponseResult.error(ResultStatusCodeEnum.DATA_NO_EXIST);
    }

    /**
     * 添加参数配置项
     * @param systemConfig
     * @return
     */
    @RepeatSubmit
    @PostMapping("add")
    @Log(title = "参数配置", businessType = BusinessType.INSERT)
    @PreAuthorize("@permission.hasPermission('system:config:add')")
    public ResponseResult add(@RequestBody SystemConfig systemConfig){
        if(systemConfig == null){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        SystemConfig temp = systemConfigService.selectOneByKey(systemConfig.getK());
        if(temp != null){
            return ResponseResult.error(ResultStatusCodeEnum.DATA_EXIST);
        }
        systemConfig.setId(IdUtil.getSnowflakeNextIdStr());

        boolean b = systemConfigService.save(systemConfig);
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }

    /**
     * 修改参数配置项
     * @param systemConfig
     * @return
     */
    @RepeatSubmit
    @PostMapping("update")
    @Log(title = "参数配置", businessType = BusinessType.UPDATE)
    @PreAuthorize("@permission.hasPermission('system:config:update')")
    public ResponseResult update(@RequestBody SystemConfig systemConfig){
        if(systemConfig == null || StrUtil.isBlank(systemConfig.getId())){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        SystemConfig temp = systemConfigService.selectOneByKey(systemConfig.getK());
        if(temp != null && !temp.getK().equals(systemConfig.getK())){
            return ResponseResult.error(ResultStatusCodeEnum.DATA_EXIST);
        }
        boolean b = systemConfigService.updateById(systemConfig);
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }

    /**
     * 修改参数配置项
     * @param systemConfigs
     * @return
     */
    @RepeatSubmit
    @PostMapping("updateBatch")
    @Log(title = "参数配置", businessType = BusinessType.UPDATE)
    @PreAuthorize("@permission.hasPermission('system:config:update')")
    public ResponseResult updateBatch(@RequestBody List<SystemConfig> systemConfigs){
        if(systemConfigs == null || systemConfigs.size() <= 0){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        boolean b = systemConfigService.updateBatchById(systemConfigs);
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }
}
