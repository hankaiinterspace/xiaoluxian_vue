package cn.xuexiluxian.open.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "cn.xuexiluxian.open")
public class OpenDeerApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenDeerApplication.class,args);
    }
}
