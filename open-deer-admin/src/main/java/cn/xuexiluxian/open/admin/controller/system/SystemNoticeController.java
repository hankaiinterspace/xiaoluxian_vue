package cn.xuexiluxian.open.admin.controller.system;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.xuexiluxian.open.common.annotation.Log;
import cn.xuexiluxian.open.common.annotation.RepeatSubmit;
import cn.xuexiluxian.open.common.enums.BusinessType;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.common.enums.ResultStatusCodeEnum;
import cn.xuexiluxian.open.common.response.ResponseResult;
import cn.xuexiluxian.open.common.utils.SecurityUtil;
import cn.xuexiluxian.open.system.model.dto.SystemNoticeDTO;
import cn.xuexiluxian.open.system.model.entity.SystemNotice;
import cn.xuexiluxian.open.system.model.req.SystemNoticeREQ;
import cn.xuexiluxian.open.system.service.ISystemNoticeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * 通知公告
 */
@Slf4j
@RestController
@RequestMapping("system/notice")
public class SystemNoticeController {
    @Autowired
    private ISystemNoticeService systemNoticeService;
    /**
     * 分页查询通知公告
     * @param page
     * @param params
     * @return
     */
    @GetMapping(value = "page")
    @Log(title = "通知公告", businessType = BusinessType.SELECT)
    @PreAuthorize("@permission.hasPermission('system:notice:page')")
    public ResponseResult page(Page page, SystemNoticeREQ params) {
        Page<SystemNotice> pageList = systemNoticeService.pageList(page, params);
        return ResponseResult.ok(pageList);
    }

    /**
     * 删除通知公告
     * @param id
     * @return
     */
    @RepeatSubmit
    @GetMapping("delete/{id}")
    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @PreAuthorize("@permission.hasPermission('system:notice:delete')")
    public ResponseResult delete(@PathVariable String id){
        if(StrUtil.isBlank(id)){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        boolean b = systemNoticeService.removeById(id);
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }

    /**
     * 通知公告详情
     * @param id
     * @return
     */
    @GetMapping("get/{id}")
    @Log(title = "通知公告", businessType = BusinessType.SELECT)
    @PreAuthorize("@permission.hasPermission('system:notice:get')")
    public ResponseResult get(@PathVariable String id){
        if(StrUtil.isBlank(id)){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        SystemNotice systemNotice = systemNoticeService.getById(id);
        if(systemNotice != null){
            return ResponseResult.ok(systemNotice);
        }
        return ResponseResult.error(ResultStatusCodeEnum.DATA_NO_EXIST);
    }

    /**
     * 添加通知公告
     * @param systemNoticeDTO
     * @return
     */
    @RepeatSubmit
    @PostMapping("add")
    @Log(title = "通知公告", businessType = BusinessType.INSERT)
    @PreAuthorize("@permission.hasPermission('system:notice:add')")
    public ResponseResult add(@RequestBody SystemNoticeDTO systemNoticeDTO){
        String userId = SecurityUtil.getUserId();

        SystemNotice systemNotice = new SystemNotice();
        BeanUtils.copyProperties(systemNoticeDTO, systemNotice);
        systemNotice.setId(IdUtil.getSnowflakeNextIdStr());
        systemNotice.setCreateBy(userId);
        systemNotice.setCreateTime(new Date());

        boolean b = systemNoticeService.save(systemNotice);
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }

    /**
     * 修改通知公告
     * @param systemNoticeDTO
     * @return
     */
    @RepeatSubmit
    @PostMapping("update")
    @Log(title = "通知公告", businessType = BusinessType.UPDATE)
    @PreAuthorize("@permission.hasPermission('system:notice:update')")
    public ResponseResult update(@RequestBody SystemNoticeDTO systemNoticeDTO){
        String userId = SecurityUtil.getUserId();
        if(systemNoticeDTO == null || StrUtil.isBlank(systemNoticeDTO.getId())){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        SystemNotice systemNotice = new SystemNotice();
        BeanUtils.copyProperties(systemNoticeDTO, systemNotice);
        systemNotice.setUpdateBy(userId);
        systemNotice.setUpdateTime(new Date());

        boolean b = systemNoticeService.updateById(systemNotice);
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }
}
