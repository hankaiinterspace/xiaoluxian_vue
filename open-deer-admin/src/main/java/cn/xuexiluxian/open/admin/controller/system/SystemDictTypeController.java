package cn.xuexiluxian.open.admin.controller.system;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.xuexiluxian.open.common.annotation.Log;
import cn.xuexiluxian.open.common.annotation.RepeatSubmit;
import cn.xuexiluxian.open.common.enums.BusinessType;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.common.enums.ResultStatusCodeEnum;
import cn.xuexiluxian.open.common.response.ResponseResult;
import cn.xuexiluxian.open.common.utils.SecurityUtil;
import cn.xuexiluxian.open.system.model.dto.SystemDictTypeDTO;
import cn.xuexiluxian.open.system.model.entity.SystemDictType;
import cn.xuexiluxian.open.system.model.req.SystemDictTypeREQ;
import cn.xuexiluxian.open.system.service.ISystemDictTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * 字典类型管理
 */
@Slf4j
@RestController
@RequestMapping("system/dict/type")
public class SystemDictTypeController {
    @Autowired
    private ISystemDictTypeService systemDictTypeService;

    /**
     * 分页查询字典类型
     * @param page
     * @param params
     * @return
     */
    @GetMapping(value = "page")
    @Log(title = "字典类型管理", businessType = BusinessType.SELECT)
    @PreAuthorize("@permission.hasPermission('system:dicttype:page')")
    public ResponseResult page(Page page, SystemDictTypeREQ params) {
        Page<SystemDictType> pageList = systemDictTypeService.pageList(page, params);
        return ResponseResult.ok(pageList);
    }

    /**
     * 删除字典类型
     * @param id
     * @return
     */
    @RepeatSubmit
    @GetMapping("delete/{id}")
    @Log(title = "字典类型管理", businessType = BusinessType.DELETE)
    @PreAuthorize("@permission.hasPermission('system:dicttype:delete')")
    public ResponseResult delete(@PathVariable String id){
        if(StrUtil.isBlank(id)){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        boolean b = systemDictTypeService.removeById(id);
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }

    /**
     * 字典类型详情
     * @param id
     * @return
     */
    @GetMapping("get/{id}")
    @Log(title = "字典类型管理", businessType = BusinessType.SELECT)
    @PreAuthorize("@permission.hasPermission('system:dicttype:get')")
    public ResponseResult get(@PathVariable String id){
        if(StrUtil.isBlank(id)){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        SystemDictType systemDictType = systemDictTypeService.getById(id);
        if(systemDictType != null){
            return ResponseResult.ok(systemDictType);
        }
        return ResponseResult.error(ResultStatusCodeEnum.DATA_NO_EXIST);
    }

    /**
     * 添加字典类型
     * @param systemDictTypeDTO
     * @return
     */
    @RepeatSubmit
    @PostMapping("add")
    @Log(title = "字典类型管理", businessType = BusinessType.INSERT)
    @PreAuthorize("@permission.hasPermission('system:dicttype:add')")
    public ResponseResult add(@RequestBody SystemDictTypeDTO systemDictTypeDTO){
        String userId = SecurityUtil.getUserId();

        SystemDictType systemDictType = new SystemDictType();
        BeanUtils.copyProperties(systemDictTypeDTO, systemDictType);
        systemDictType.setId(IdUtil.getSnowflakeNextIdStr());
        systemDictType.setCreateBy(userId);
        systemDictType.setCreateTime(new Date());

        boolean b = systemDictTypeService.save(systemDictType);
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }

    /**
     * 修改字典类型
     * @param systemDictTypeDTO
     * @return
     */
    @RepeatSubmit
    @PostMapping("update")
    @Log(title = "字典类型管理", businessType = BusinessType.UPDATE)
    @PreAuthorize("@permission.hasPermission('system:dicttype:update')")
    public ResponseResult update(@RequestBody SystemDictTypeDTO systemDictTypeDTO){
        String userId = SecurityUtil.getUserId();
        if(systemDictTypeDTO == null || StrUtil.isBlank(systemDictTypeDTO.getId())){
            return ResponseResult.error(ResultStatusCodeEnum.PARAM_REQUIRE_ERROR);
        }
        SystemDictType systemDictType = new SystemDictType();
        BeanUtils.copyProperties(systemDictTypeDTO, systemDictType);
        systemDictType.setUpdateBy(userId);
        systemDictType.setUpdateTime(new Date());

        boolean b = systemDictTypeService.updateById(systemDictType);
        if(b){
            return ResponseResult.ok();
        }
        return ResponseResult.error(ResultStatusCodeEnum.HTTP_ERROR);
    }
}
