package cn.xuexiluxian.open.aes.test;

import cn.hutool.json.JSONConfig;
import cn.hutool.json.JSONUtil;
import cn.xuexiluxian.open.common.utils.AesUtil;
import cn.xuexiluxian.open.system.model.dto.SystemMenuDTO;

public class TestAes {
    public static void main(String[] args) {
        System.out.println(AesUtil.encrypt("admin"));
        System.out.println(AesUtil.encrypt("123456"));


        JSONConfig jsonConfig = new JSONConfig();
        jsonConfig.setIgnoreNullValue(false);
        System.out.println(JSONUtil.toJsonStr(new SystemMenuDTO(), jsonConfig));
    }
}
