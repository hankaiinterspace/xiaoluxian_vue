/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.7.35 : Database - open-deer
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`open-deer` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `open-deer`;

/*Table structure for table `qrtz_blob_triggers` */

DROP TABLE IF EXISTS `qrtz_blob_triggers`;

CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `qrtz_blob_triggers` */

/*Table structure for table `qrtz_calendars` */

DROP TABLE IF EXISTS `qrtz_calendars`;

CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `CALENDAR_NAME` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `qrtz_calendars` */

/*Table structure for table `qrtz_cron_triggers` */

DROP TABLE IF EXISTS `qrtz_cron_triggers`;

CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `CRON_EXPRESSION` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TIME_ZONE_ID` varchar(80) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `qrtz_cron_triggers` */

insert  into `qrtz_cron_triggers`(`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`CRON_EXPRESSION`,`TIME_ZONE_ID`) values ('TaskScheduler','TASK_CLASS_NAME1552895359250931712','测试','1/1 * * * * ?','Asia/Shanghai'),('TaskScheduler','TASK_CLASS_NAME1554347302003507200','备份恢复','0 15 10 ? * *','Asia/Shanghai');

/*Table structure for table `qrtz_fired_triggers` */

DROP TABLE IF EXISTS `qrtz_fired_triggers`;

CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `ENTRY_ID` varchar(95) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `INSTANCE_NAME` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `JOB_NAME` varchar(190) COLLATE utf8mb4_bin DEFAULT NULL,
  `JOB_GROUP` varchar(190) COLLATE utf8mb4_bin DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `qrtz_fired_triggers` */

/*Table structure for table `qrtz_job_details` */

DROP TABLE IF EXISTS `qrtz_job_details`;

CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `JOB_NAME` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `JOB_GROUP` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `DESCRIPTION` varchar(250) COLLATE utf8mb4_bin DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) COLLATE utf8mb4_bin NOT NULL,
  `IS_DURABLE` varchar(1) COLLATE utf8mb4_bin NOT NULL,
  `IS_NONCONCURRENT` varchar(1) COLLATE utf8mb4_bin NOT NULL,
  `IS_UPDATE_DATA` varchar(1) COLLATE utf8mb4_bin NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) COLLATE utf8mb4_bin NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `qrtz_job_details` */

insert  into `qrtz_job_details`(`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`,`DESCRIPTION`,`JOB_CLASS_NAME`,`IS_DURABLE`,`IS_NONCONCURRENT`,`IS_UPDATE_DATA`,`REQUESTS_RECOVERY`,`JOB_DATA`) values ('TaskScheduler','TASK_CLASS_NAME1552895359250931712','测试',NULL,'cn.xuexiluxian.open.quartz.util.QuartzDisallowConcurrentExecution','0','1','0','0','��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0TASK_PROPERTIESsr\0+cn.xuexiluxian.open.quartz.entity.SystemJob%}����c\0L\0\nconcurrentt\0Ljava/lang/Integer;L\0cronExpressiont\0Ljava/lang/String;L\0idq\0~\0\nL\0invokeTargetq\0~\0\nL\0jobGroupq\0~\0\nL\0jobNameq\0~\0\nL\0\rmisfirePolicyq\0~\0\nL\0statusq\0~\0	xr\0,cn.xuexiluxian.open.common.entity.BaseEntity�.(����,\0L\0createByq\0~\0\nL\0\ncreateTimet\0Ljava/util/Date;L\0updateByq\0~\0\nL\0\nupdateTimeq\0~\0xpt\01sr\0java.util.Datehj�KYt\0\0xpw\0\0�H�m\0xt\01sq\0~\0w\0\0�g̓Pxsr\0java.lang.Integer⠤���8\0I\0valuexr\0java.lang.Number������\0\0xp\0\0\0t\0\r1/1 * * * * ?t\01552895359250931712t\03cn.xuexiluxian.open.quartz.task.DemoTask.noParams()t\0测试t\0测试12t\02sq\0~\0\0\0\0\0x\0'),('TaskScheduler','TASK_CLASS_NAME1554347302003507200','备份恢复',NULL,'cn.xuexiluxian.open.quartz.util.QuartzDisallowConcurrentExecution','0','1','0','0','��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0TASK_PROPERTIESsr\0+cn.xuexiluxian.open.quartz.entity.SystemJob%}����c\0L\0\nconcurrentt\0Ljava/lang/Integer;L\0cronExpressiont\0Ljava/lang/String;L\0idq\0~\0\nL\0invokeTargetq\0~\0\nL\0jobGroupq\0~\0\nL\0jobNameq\0~\0\nL\0\rmisfirePolicyq\0~\0\nL\0statusq\0~\0	xr\0,cn.xuexiluxian.open.common.entity.BaseEntity�.(����,\0L\0createByq\0~\0\nL\0\ncreateTimet\0Ljava/util/Date;L\0updateByq\0~\0\nL\0\nupdateTimeq\0~\0xpt\01sr\0java.util.Datehj�KYt\0\0xpw\0\0�8%�(xt\01sq\0~\0w\0\0�8(��xsr\0java.lang.Integer⠤���8\0I\0valuexr\0java.lang.Number������\0\0xp\0\0\0t\0\r0 15 10 ? * *t\01554347302003507200t\06cn.xuexiluxian.open.quartz.task.DemoTask.params(\"abc\")t\0备份恢复t\0数据库备份t\02q\0~\0x\0');

/*Table structure for table `qrtz_locks` */

DROP TABLE IF EXISTS `qrtz_locks`;

CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `LOCK_NAME` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `qrtz_locks` */

insert  into `qrtz_locks`(`SCHED_NAME`,`LOCK_NAME`) values ('TaskScheduler','STATE_ACCESS'),('TaskScheduler','TRIGGER_ACCESS');

/*Table structure for table `qrtz_paused_trigger_grps` */

DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;

CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `qrtz_paused_trigger_grps` */

/*Table structure for table `qrtz_scheduler_state` */

DROP TABLE IF EXISTS `qrtz_scheduler_state`;

CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `INSTANCE_NAME` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `qrtz_scheduler_state` */

insert  into `qrtz_scheduler_state`(`SCHED_NAME`,`INSTANCE_NAME`,`LAST_CHECKIN_TIME`,`CHECKIN_INTERVAL`) values ('TaskScheduler','DESKTOP-3LAT2DB1660177281403',1660185477341,15000);

/*Table structure for table `qrtz_simple_triggers` */

DROP TABLE IF EXISTS `qrtz_simple_triggers`;

CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `qrtz_simple_triggers` */

/*Table structure for table `qrtz_simprop_triggers` */

DROP TABLE IF EXISTS `qrtz_simprop_triggers`;

CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `STR_PROP_1` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `STR_PROP_2` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `STR_PROP_3` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `qrtz_simprop_triggers` */

/*Table structure for table `qrtz_triggers` */

DROP TABLE IF EXISTS `qrtz_triggers`;

CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_NAME` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_GROUP` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `JOB_NAME` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `JOB_GROUP` varchar(190) COLLATE utf8mb4_bin NOT NULL,
  `DESCRIPTION` varchar(250) COLLATE utf8mb4_bin DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) COLLATE utf8mb4_bin NOT NULL,
  `TRIGGER_TYPE` varchar(8) COLLATE utf8mb4_bin NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(190) COLLATE utf8mb4_bin DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `qrtz_triggers` */

insert  into `qrtz_triggers`(`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`JOB_NAME`,`JOB_GROUP`,`DESCRIPTION`,`NEXT_FIRE_TIME`,`PREV_FIRE_TIME`,`PRIORITY`,`TRIGGER_STATE`,`TRIGGER_TYPE`,`START_TIME`,`END_TIME`,`CALENDAR_NAME`,`MISFIRE_INSTR`,`JOB_DATA`) values ('TaskScheduler','TASK_CLASS_NAME1552895359250931712','测试','TASK_CLASS_NAME1552895359250931712','测试',NULL,1660177281000,-1,5,'PAUSED','CRON',1660177281000,0,NULL,1,''),('TaskScheduler','TASK_CLASS_NAME1554347302003507200','备份恢复','TASK_CLASS_NAME1554347302003507200','备份恢复',NULL,1660270500000,1660184100000,5,'WAITING','CRON',1660177281000,0,NULL,1,'');

/*Table structure for table `system_config` */

DROP TABLE IF EXISTS `system_config`;

CREATE TABLE `system_config` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
  `k` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '键',
  `v` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '值',
  `type` int(11) DEFAULT '1' COMMENT '类型（1：系统内置；2：自定义）',
  `enabled` int(11) DEFAULT '1' COMMENT '是否启用（0：禁用；1：启用）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_config` */

insert  into `system_config`(`id`,`k`,`v`,`type`,`enabled`) values ('402847035e527049015e527178950003','system.logo','',1,1),('402847035fbf85e2015fbf8c23510002','upload.file.name','hex62',1,1),('402847035fbf85e2015fbf8c246f0003','system.login.theme','default',1,1),('402847035fe8ff2d015fe90173240003','system.captcha.length','',1,1),('402847035fe8ff2d015fe90173630004','system.captcha.enable','true',1,1),('402847035fe8ff2d015fe90173630005','system.captcha.type','random',1,1),('4028812662dbbf010162dbbfb59e0002','system.default.pwd','123456',1,1),('402881315fb3f381015fb406468a0007','system.unit.role','false',1,1),('4028b8815885ab62015885c969b2000f','system.login.unique','false',1,1),('4028b8815aac0f34015aac0fe0770003','system.index.page','index-tabs',1,1),('4028b8815b034cb3015b03513ec40006','system.unit.res','false',1,1),('8a80868853a692ad0153a6ab869a0014','system.page.rows','10',1,1),('8a80868853a692ad0153a6ab869a0017','upload.folder.root','${project.baseDir}/resources/upload/',1,1),('8a80868853a692ad0153a6ab869a0018','upload.remote.enabled','false',1,1),('8a80868853a692ad0153a6ab869a0020','upload.remote.view','',1,1);

/*Table structure for table `system_dict_item` */

DROP TABLE IF EXISTS `system_dict_item`;

CREATE TABLE `system_dict_item` (
  `id` varchar(32) CHARACTER SET utf8 NOT NULL,
  `type_id` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT '字典类型ID',
  `k` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '键',
  `v` varchar(128) CHARACTER SET utf8 NOT NULL COMMENT '值',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `remark` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) CHARACTER SET utf8 NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_dict_item` */

insert  into `system_dict_item`(`id`,`type_id`,`k`,`v`,`sort`,`remark`,`create_by`,`create_time`,`update_by`,`update_time`) values ('1551412251934666752','1551401265425100800','启用','1',1,'暂无111','1','2022-07-27 17:06:59','1','2022-07-27 17:07:00'),('1551412289356247040','1551401265425100800','禁用','0',2,NULL,'1','2022-08-05 10:42:44','1','2022-08-05 10:42:45'),('1553203794051895296','1548860723621392384','男','1',1,'男','1','2022-08-02 16:33:41','1','2022-08-02 16:33:41'),('1553203895239479296','1548860723621392384','女','2',2,'女','1','2022-08-02 16:32:51','1','2022-08-02 16:32:51'),('1553203929104289792','1548860723621392384','未知','0',3,'未知','1','2022-08-02 16:32:53','1','2022-08-02 16:32:54'),('1553204469536165888','1553204330402713600','目录','0',1,'目录','1','2022-07-30 10:22:57',NULL,NULL),('1553204523311337472','1553204330402713600','菜单','1',2,'菜单','1','2022-07-30 10:23:10',NULL,NULL),('1553204630131871744','1553204330402713600','按钮','2',3,'按钮','1','2022-07-30 10:23:36',NULL,NULL),('1553204880108195840','1553204792057171968','显示','0',1,'显示','1','2022-07-30 10:24:35',NULL,NULL),('1553204913180282880','1553204792057171968','隐藏','1',2,'隐藏','1','2022-07-30 10:24:43',NULL,NULL),('1553205056868749312','1553204999423561728','缓存','0',1,'缓存','1','2022-07-30 10:25:17',NULL,NULL),('1553205099587735552','1553204999423561728','不缓存','1',2,'不缓存','1','2022-07-30 10:25:28',NULL,NULL),('1553205532532183040','1553205474554318848','是','1',1,'是','1','2022-07-30 10:27:11',NULL,NULL),('1553205564010434560','1553205474554318848','否','0',2,'否','1','2022-08-01 16:14:36','1','2022-08-01 16:14:36'),('1553205779924815872','1553205712136474624','通知','1',1,'通知','1','2022-07-30 10:28:10',NULL,NULL),('1553205812569083904','1553205712136474624','公告','2',2,'公告','1','2022-07-30 10:28:18',NULL,NULL),('1553206253193302016','1553206047554965504','查询','SELECT',1,'查询','1','2022-07-30 10:30:03',NULL,NULL),('1553206333992374272','1553206047554965504','新增','INSERT',2,'新增','1','2022-07-30 10:30:22',NULL,NULL),('1553206372210872320','1553206047554965504','修改','UPDATE',3,'修改','1','2022-07-30 10:30:31',NULL,NULL),('1553206408642596864','1553206047554965504','删除','DELETE',1,'删除','1','2022-07-30 10:30:40',NULL,NULL),('1553206472496680960','1553206047554965504','授权','GRANT',5,'授权','1','2022-07-30 10:30:55',NULL,NULL),('1553206524376027136','1553206047554965504','导入','IMPORT',6,'导入','1','2022-07-30 10:31:07',NULL,NULL),('1553206563571798016','1553206047554965504','导出','EXPORT',7,'导出','1','2022-07-30 10:31:17',NULL,NULL),('1553206616516497408','1553206047554965504','清空数据','CLEAN',8,'清空数据','1','2022-07-30 10:31:29',NULL,NULL),('1553206664562249728','1553206047554965504','强退','FORCE',9,'强退','1','2022-07-30 10:31:41',NULL,NULL),('1553206729586544640','1553206047554965504','其它','OTHER',10,'其它','1','2022-07-30 10:31:56',NULL,NULL),('1553207193577230336','1553207105375211520','正常','0',1,'正常','1','2022-07-30 10:33:47',NULL,NULL),('1553207229891514368','1553207105375211520','异常','1',2,'异常','1','2022-07-30 10:33:55',NULL,NULL),('1553207653004513280','1553207513824923648','立即执行','1',1,'立即执行','1','2022-07-30 10:35:36',NULL,NULL),('1553207686072406016','1553207513824923648','执行一次','2',2,'执行一次','1','2022-07-30 10:35:44',NULL,NULL),('1553207718330798080','1553207513824923648','放弃执行','3',3,'放弃执行','1','2022-07-30 10:35:52',NULL,NULL),('1553208062754459648','1553208007335120896','允许','0',1,'允许','1','2022-07-30 10:37:14',NULL,NULL),('1553208104840105984','1553208007335120896','禁止','1',2,'禁止','1','2022-07-30 10:37:24',NULL,NULL),('1554000818120699904','1553207513824923648','默认','0',4,'默认','1','2022-08-01 15:07:22',NULL,NULL),('1554015814699462656','1554015750740520960','系统内置','1',1,'系统内置','1','2022-08-01 16:06:57',NULL,NULL),('1554015864360022016','1554015750740520960','自定义','2',2,'自定义','1','2022-08-01 16:07:09',NULL,NULL),('1555097851397148672','1555097803582083072','运行','1',1,'运行','1','2022-08-04 15:46:35',NULL,NULL),('1555097889036832768','1555097803582083072','暂停','0',2,'暂停','1','2022-08-04 15:46:44',NULL,NULL);

/*Table structure for table `system_dict_type` */

DROP TABLE IF EXISTS `system_dict_type`;

CREATE TABLE `system_dict_type` (
  `id` varchar(32) CHARACTER SET utf8 NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT '字典类型名称',
  `type` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '分类',
  `remarks` varchar(512) CHARACTER SET utf8 DEFAULT NULL COMMENT '描述',
  `create_by` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_dict_type` */

insert  into `system_dict_type`(`id`,`name`,`type`,`remarks`,`create_by`,`create_time`,`update_by`,`update_time`) values ('1548860723621392384','全局性别','system_global_gender','全局性别','1','2022-07-30 10:19:39','1','2022-07-30 10:19:39'),('1551401265425100800','全局状态','system_global_status','全局状态','1','2022-07-30 10:21:17','1','2022-07-30 10:21:17'),('1553204330402713600','菜单类型','system_menu_type','菜单类型','1','2022-07-30 10:22:24',NULL,NULL),('1553204792057171968','全局显示隐藏','system_global_visibility','全局显示隐藏','1','2022-08-01 11:12:04','1','2022-08-01 11:12:04'),('1553204999423561728','全局是否缓存','system_global_cache','全局是否缓存','1','2022-07-30 10:25:04',NULL,NULL),('1553205474554318848','全局是否','system_global_yesorno','全局是否','1','2022-07-30 10:26:57',NULL,NULL),('1553205712136474624','通知公告类型','system_notice_type','通知公告类型','1','2022-07-30 10:27:54',NULL,NULL),('1553206047554965504','日志业务类型','system_operator_type','日志业务类型','1','2022-07-30 10:29:14',NULL,NULL),('1553207105375211520','全局异常状态','system_global_expstatus','全局异常状态','1','2022-08-01 17:07:58',NULL,NULL),('1553207513824923648','执行策略','system_misfire_policy','执行策略','1','2022-07-30 10:35:03',NULL,NULL),('1553208007335120896','全局允许禁止','system_allow_prohibit','全局允许禁止','1','2022-07-30 10:37:01',NULL,NULL),('1554015750740520960','定时任务分组','system_global_taskgroup','定时任务分组','1','2022-08-01 16:06:42',NULL,NULL),('1555097803582083072','定时任务状态','system_task_status','定时任务状态','1','2022-08-04 15:46:23',NULL,NULL);

/*Table structure for table `system_job` */

DROP TABLE IF EXISTS `system_job`;

CREATE TABLE `system_job` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '任务ID',
  `job_name` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) COLLATE utf8mb4_bin NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) COLLATE utf8mb4_bin DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` tinyint(1) DEFAULT '1' COMMENT '是否并发执行（1允许 0禁止）',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态（1运行 0暂停）',
  `del_flag` tinyint(1) DEFAULT '0' COMMENT '是否删除:0有效,1删除',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='定时任务调度表';

/*Data for the table `system_job` */

insert  into `system_job`(`id`,`job_name`,`job_group`,`invoke_target`,`cron_expression`,`misfire_policy`,`concurrent`,`status`,`del_flag`,`create_by`,`create_time`,`update_by`,`update_time`) values ('1552895359250931712','测试12','测试','cn.xuexiluxian.open.quartz.task.DemoTask.noParams()','1/1 * * * * ?','2',1,0,0,'1','2022-07-29 13:54:40','1','2022-08-04 15:41:38'),('1554347302003507200','数据库备份','备份恢复','cn.xuexiluxian.open.quartz.task.DemoTask.params(\"abc\")','0 15 10 ? * *','2',1,1,0,'1','2022-07-26 09:36:09','1','2022-07-26 09:39:15');

/*Table structure for table `system_job_log` */

DROP TABLE IF EXISTS `system_job_log`;

CREATE TABLE `system_job_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) COLLATE utf8mb4_bin NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '日志信息',
  `status` tinyint(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) COLLATE utf8mb4_bin DEFAULT '' COMMENT '异常信息',
  `start_time` datetime DEFAULT NULL COMMENT '创建时间',
  `stop_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='定时任务调度日志表';

/*Data for the table `system_job_log` */

/*Table structure for table `system_login_log` */

DROP TABLE IF EXISTS `system_login_log`;

CREATE TABLE `system_login_log` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT 'id',
  `user_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '操作系统',
  `msg` varchar(255) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_login_log` */

/*Table structure for table `system_menu` */

DROP TABLE IF EXISTS `system_menu`;

CREATE TABLE `system_menu` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '菜单ID',
  `name` varchar(128) CHARACTER SET utf8mb4 NOT NULL COMMENT '菜单名称',
  `parent_id` varchar(32) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '父菜单ID',
  `sort` int(11) DEFAULT '0' COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '路由地址',
  `query` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '路由参数',
  `component` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '组件路径',
  `cache` int(1) DEFAULT '0' COMMENT '是否缓存（0：缓存；1：不缓存）',
  `type` int(1) DEFAULT NULL COMMENT '菜单类型（0：目录；1：菜单；2：按钮）',
  `redirect` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '重定向',
  `visible` int(1) DEFAULT '0' COMMENT '显示状态（0：显示；1：隐藏）',
  `enabled` int(1) DEFAULT '0' COMMENT '菜单状态（0：禁用；1：启用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 DEFAULT '#' COMMENT '菜单图标',
  `remark` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_menu` */

insert  into `system_menu`(`id`,`name`,`parent_id`,`sort`,`path`,`query`,`component`,`cache`,`type`,`redirect`,`visible`,`enabled`,`perms`,`icon`,`remark`,`create_by`,`create_time`,`update_by`,`update_time`) values ('1552831340867551232','系统监控','-1',3,'/monitor','{}',NULL,0,0,'/monitor/server',0,1,NULL,'Monitor',NULL,'1','2022-07-29 09:40:17','1','2022-08-03 13:57:50'),('1552832554816569344','系统管理','-1',2,'/system','{}',NULL,0,0,'/system/user',0,1,NULL,'Setting',NULL,'1','2022-07-29 09:45:06','1','2022-08-03 13:57:46'),('1552834825520480256','用户管理','1552832554816569344',1,'/system/user','{}',NULL,0,1,NULL,0,1,'system:user','User',NULL,'1','2022-07-29 09:54:07','1','2022-08-05 10:25:16'),('1552837943025008640','角色管理','1552832554816569344',2,'/system/role','{}',NULL,0,1,NULL,0,1,'system:role','Avatar',NULL,'1','2022-07-29 10:06:31','1','2022-08-05 10:24:43'),('1552838255668428800','菜单管理','1552832554816569344',3,'/system/menu','{}',NULL,0,1,NULL,0,1,'system:menu','Menu',NULL,'1','2022-07-29 10:07:45','1','2022-08-05 10:25:30'),('1552840272302374912','机构管理','1552832554816569344',4,'/system/department','{}',NULL,0,1,NULL,0,1,'system:unit','Postcard',NULL,'1','2022-07-29 10:15:46','1','2022-08-05 10:26:07'),('1552840644655906816','岗位管理','1552832554816569344',5,'/system/post','{}',NULL,0,1,NULL,0,1,'system:post','Baseball',NULL,'1','2022-07-29 10:17:15','1','2022-08-05 10:26:19'),('1552840927784009728','字典管理','1552832554816569344',6,'/system/dict','{}',NULL,0,1,NULL,0,1,'system:dicttype','Notebook',NULL,'1','2022-07-29 10:18:22','1','2022-08-10 16:57:55'),('1552841237894070272','参数设置','1552832554816569344',7,'/system/params','{}',NULL,0,1,NULL,0,1,'system:config','BottomLeft',NULL,'1','2022-07-29 10:19:36','1','2022-08-05 10:27:07'),('1552841910891118592','通知公告','1552832554816569344',8,'/system/notice','{}',NULL,0,1,NULL,0,1,'system:notice','Box',NULL,'1','2022-07-29 10:22:17','1','2022-08-05 10:27:17'),('1552842207126421504','日志管理','1552832554816569344',9,'/system/logger','{}',NULL,0,0,'/system/logger/operlog',0,1,NULL,'CreditCard',NULL,'1','2022-07-29 10:23:27','1','2022-07-29 11:36:06'),('1552842481920442368','操作日志','1552842207126421504',1,'/system/logger/operlog','{}',NULL,1,1,NULL,0,1,NULL,'Collection',NULL,'1','2022-07-29 10:24:33','1','2022-08-10 13:56:46'),('1552842905746472960','登录日志','1552842207126421504',2,'/system/logger/loginlog','{}',NULL,1,1,NULL,0,1,NULL,'DocumentCopy',NULL,'1','2022-07-29 10:26:14','1','2022-08-10 13:58:29'),('1552843150869987328','服务监控','1552831340867551232',1,'/monitor/server','{}',NULL,0,1,NULL,0,1,'monitor:server','DataLine',NULL,'1','2022-07-29 10:27:12','1','2022-08-10 14:02:29'),('1552843370894786560','缓存监控','1552831340867551232',2,'/monitor/cache','{}',NULL,0,1,NULL,0,1,'monitor:cache','Files',NULL,'1','2022-07-29 10:28:05','1','2022-08-10 14:04:21'),('1552843537425432576','定时任务','1552831340867551232',3,'/monitor/timing','{}',NULL,0,1,NULL,0,1,'task:job','AddLocation',NULL,'1','2022-07-29 10:28:44','1','2022-08-05 10:28:31'),('1553289085710274560','角色授权','1552832554816569344',2,'/system/role-assigned','{}',NULL,0,1,'',0,1,'system:role:assigned','Coordinate',NULL,'1','2022-07-30 15:59:11','1','2022-08-05 10:25:07'),('1553289562300649472','字典项管理','1552832554816569344',6,'/system/dict-item','{}',NULL,0,1,'',0,1,'system:dictitem','SetUp',NULL,'1','2022-07-30 16:01:05','1','2022-08-10 13:54:08'),('1554660092320739328','小鹿线','-1',4,'http://www.xuexiluxian.cn','{}',NULL,0,1,'',0,1,'','TopRight',NULL,'1','2022-08-03 10:47:05','1','2022-08-04 16:28:23'),('1555021778256936960','用户分页列表','1552834825520480256',1,'','{}',NULL,0,2,NULL,0,1,'system:user:page','IconEmpty',NULL,'1','2022-08-04 10:44:18','1','2022-08-05 11:53:35'),('1555022690253815808','删除用户','1552834825520480256',2,'','{}',NULL,0,2,NULL,0,1,'system:user:delete','IconEmpty',NULL,'1','2022-08-04 10:47:55','1','2022-08-05 10:31:31'),('1555022805899165696','用户详情','1552834825520480256',5,'','{}',NULL,0,2,NULL,0,1,'system:user:get','IconEmpty',NULL,'1','2022-08-04 10:48:23','1','2022-08-05 10:33:09'),('1555022916632985600','添加用户','1552834825520480256',4,'','{}',NULL,0,2,NULL,0,1,'system:user:add','IconEmpty',NULL,'1','2022-08-04 10:48:49','1','2022-08-05 10:33:07'),('1555023027287113728','修改用户','1552834825520480256',3,'','{}',NULL,0,2,NULL,0,1,'system:user:update','IconEmpty',NULL,'1','2022-08-04 10:49:15','1','2022-08-05 10:32:55'),('1555023414698196992','角色分页列表','1552837943025008640',1,'','{}',NULL,0,2,NULL,0,1,'system:role:page','IconEmpty',NULL,'1','2022-08-04 10:50:48','1','2022-08-05 10:33:25'),('1555024916816871424','删除角色','1552837943025008640',2,'','{}',NULL,0,2,NULL,0,1,'system:role:delete','IconEmpty',NULL,'1','2022-08-04 10:56:46','1','2022-08-05 10:33:28'),('1555025013080342528','角色详情','1552837943025008640',5,'','{}',NULL,0,2,NULL,0,1,'system:role:get','IconEmpty',NULL,'1','2022-08-04 10:57:09','1','2022-08-05 10:33:35'),('1555025109608054784','添加角色','1552837943025008640',4,'','{}',NULL,0,2,NULL,0,1,'system:role:add','IconEmpty',NULL,'1','2022-08-04 10:57:32','1','2022-08-05 10:33:32'),('1555025221142986752','修改角色','1552837943025008640',3,'','{}',NULL,0,2,NULL,0,1,'system:role:update','IconEmpty',NULL,'1','2022-08-04 10:57:58','1','2022-08-05 10:33:30'),('1555025365808726016','全部角色','1552837943025008640',6,'','{}',NULL,0,2,NULL,0,1,'system:role:page','IconEmpty',NULL,'1','2022-08-04 10:58:33','1','2022-08-05 10:33:37'),('1555025467013087232','已授权用户','1553289085710274560',1,'','{}',NULL,0,2,NULL,0,1,'system:role:assigned','IconEmpty',NULL,'1','2022-08-04 10:58:57','1','2022-08-05 10:36:06'),('1555025565591814144','取消用户授权','1553289085710274560',2,'','{}',NULL,0,2,NULL,0,1,'system:role:grant','IconEmpty',NULL,'1','2022-08-04 10:59:20','1','2022-08-05 10:36:08'),('1555026583431954432','岗位分页列表','1552840644655906816',1,'','{}',NULL,0,2,NULL,0,1,'system:post:page','IconEmpty',NULL,'1','2022-08-04 11:03:23','1','2022-08-05 10:36:49'),('1555026703904948224','删除岗位','1552840644655906816',2,'','{}',NULL,0,2,NULL,0,1,'system:post:delete','IconEmpty',NULL,'1','2022-08-04 11:03:52','1','2022-08-05 10:36:51'),('1555026797555367936','岗位详情','1552840644655906816',5,'','{}',NULL,0,2,NULL,0,1,'system:post:get','IconEmpty',NULL,'1','2022-08-04 11:04:14','1','2022-08-05 10:36:57'),('1555026932775534592','添加岗位','1552840644655906816',4,'','{}',NULL,0,2,NULL,0,1,'system:post:add','IconEmpty',NULL,'1','2022-08-04 11:04:46','1','2022-08-05 10:36:55'),('1555027058961170432','修改岗位','1552840644655906816',3,'','{}',NULL,0,2,NULL,0,1,'system:post:update','IconEmpty',NULL,'1','2022-08-04 11:05:17','1','2022-08-05 10:36:53'),('1555027214834089984','全部岗位','1552840644655906816',6,'','{}',NULL,0,2,NULL,0,1,'system:post:page','IconEmpty',NULL,'1','2022-08-04 11:05:54','1','2022-08-05 10:36:59'),('1555028623730163712','机构分页列表','1552840272302374912',1,'','{}',NULL,0,2,NULL,0,1,'system:unit:list','IconEmpty',NULL,'1','2022-08-04 11:11:30','1','2022-08-05 10:36:31'),('1555028755930431488','机构列表树','1552840272302374912',2,'','{}',NULL,0,2,NULL,0,1,'system:unit:tree','IconEmpty',NULL,'1','2022-08-04 11:12:01','1','2022-08-05 10:36:32'),('1555028860968386560','添加机构','1552840272302374912',5,'','{}',NULL,0,2,NULL,0,1,'system:unit:add','IconEmpty',NULL,'1','2022-08-04 11:12:26','1','2022-08-05 10:36:39'),('1555029121996701696','修改机构','1552840272302374912',4,'','{}',NULL,0,2,NULL,0,1,'system:unit:update','IconEmpty',NULL,'1','2022-08-04 11:13:28','1','2022-08-05 10:36:36'),('1555029229572210688','机构详情','1552840272302374912',6,'','{}',NULL,0,2,NULL,0,1,'system:unit:get','IconEmpty',NULL,'1','2022-08-04 11:13:54','1','2022-08-05 10:36:41'),('1555029352754724864','删除机构','1552840272302374912',3,'','{}',NULL,0,2,NULL,0,1,'system:unit:delete','IconEmpty',NULL,'1','2022-08-04 11:14:23','1','2022-08-05 10:36:34'),('1555029594791231488','通知公告分页列表','1552841910891118592',1,'','{}',NULL,0,2,NULL,0,1,'system:notice:page','IconEmpty',NULL,'1','2022-08-04 11:15:21','1','2022-08-05 10:38:00'),('1555030572403802112','删除通知公告','1552841910891118592',2,'','{}',NULL,0,2,NULL,0,1,'system:notice:delete','IconEmpty',NULL,'1','2022-08-04 11:19:14','1','2022-08-05 10:38:02'),('1555030762397384704','通知公告详情','1552841910891118592',5,'','{}',NULL,0,2,NULL,0,1,'system:notice:get','IconEmpty',NULL,'1','2022-08-04 11:19:59','1','2022-08-05 10:38:08'),('1555031035136196608','添加通知公告','1552841910891118592',4,'','{}',NULL,0,2,NULL,0,1,'system:notice:add','IconEmpty',NULL,'1','2022-08-04 11:21:05','1','2022-08-05 10:38:06'),('1555031176790425600','修改通知公告','1552841910891118592',3,'','{}',NULL,0,2,NULL,0,1,'system:notice:update','IconEmpty',NULL,'1','2022-08-04 11:21:38','1','2022-08-05 10:38:04'),('1555032086073589760','字典类型分页列表','1552840927784009728',1,'','{}',NULL,0,2,NULL,0,1,'system:dicttype:page','IconEmpty',NULL,'1','2022-08-04 11:25:15','1','2022-08-05 10:37:05'),('1555032190113300480','删除字典类型','1552840927784009728',2,'','{}',NULL,0,2,NULL,0,1,'system:dicttype:delete','IconEmpty',NULL,'1','2022-08-04 11:25:40','1','2022-08-05 10:37:07'),('1555032485929172992','字典类型详情','1552840927784009728',5,'','{}',NULL,0,2,NULL,0,1,'system:dicttype:get','IconEmpty',NULL,'1','2022-08-04 11:26:50','1','2022-08-05 10:37:15'),('1555032676602232832','添加字典类型','1552840927784009728',4,'','{}',NULL,0,2,NULL,0,1,'system:dicttype:add','IconEmpty',NULL,'1','2022-08-04 11:27:36','1','2022-08-05 10:37:11'),('1555032781178814464','修改字典类型','1552840927784009728',3,'','{}',NULL,0,2,NULL,0,1,'system:dicttype:update','IconEmpty',NULL,'1','2022-08-04 11:28:01','1','2022-08-05 10:37:09'),('1555034605604585472','字典项分页列表','1553289562300649472',1,'','{}',NULL,0,2,NULL,0,1,'system:dictitem:page','IconEmpty',NULL,'1','2022-08-04 11:35:16','1','2022-08-05 10:37:21'),('1555035026716901376','删除字典项','1553289562300649472',2,'','{}',NULL,0,2,NULL,0,1,'system:dictitem:delete','IconEmpty',NULL,'1','2022-08-04 11:36:56','1','2022-08-05 10:37:23'),('1555035127086596096','字典项详情','1553289562300649472',5,'','{}',NULL,0,2,NULL,0,1,'system:dictitem:get','IconEmpty',NULL,'1','2022-08-04 11:37:20','1','2022-08-05 10:37:29'),('1555035265049837568','添加字典项','1553289562300649472',4,'','{}',NULL,0,2,NULL,0,1,'system:dictitem:add','IconEmpty',NULL,'1','2022-08-04 11:37:53','1','2022-08-05 10:37:27'),('1555035435468603392','修改字典项','1553289562300649472',3,'','{}',NULL,0,2,NULL,0,1,'system:dictitem:update','IconEmpty',NULL,'1','2022-08-04 11:38:34','1','2022-08-05 10:37:25'),('1555035861660221440','菜单权限分页列表','1552838255668428800',1,'','{}',NULL,0,2,NULL,0,1,'system:menu:page','IconEmpty',NULL,'1','2022-08-04 11:40:15','1','2022-08-05 10:36:15'),('1555036052165509120','删除菜单权限','1552838255668428800',2,'','{}',NULL,0,2,NULL,0,1,'system:menu:delete','IconEmpty',NULL,'1','2022-08-04 11:41:01','1','2022-08-05 10:36:18'),('1555036136961753088','菜单权限详情','1552838255668428800',5,'','{}',NULL,0,2,NULL,0,1,'system:menu:get','IconEmpty',NULL,'1','2022-08-04 11:41:21','1','2022-08-05 10:36:24'),('1555036219384020992','添加菜单权限','1552838255668428800',4,'','{}',NULL,0,2,NULL,0,1,'system:menu:add','IconEmpty',NULL,'1','2022-08-04 11:41:41','1','2022-08-05 10:36:22'),('1555036314888323072','修改菜单权限','1552838255668428800',3,'','{}',NULL,0,2,NULL,0,1,'system:menu:update','IconEmpty',NULL,'1','2022-08-04 11:42:03','1','2022-08-05 10:36:20'),('1555036936706473984','参数分页列表','1552841237894070272',1,'','{}',NULL,0,2,NULL,0,1,'system:config:page','IconEmpty',NULL,'1','2022-08-04 11:44:32','1','2022-08-05 10:37:53'),('1555037170614419456','删除参数','1552841237894070272',2,'','{}',NULL,0,2,NULL,0,1,'system:config:delete','IconEmpty',NULL,'1','2022-08-04 11:45:27','1','2022-08-05 10:37:49'),('1555037354723393536','参数详情','1552841237894070272',6,'','{}',NULL,0,2,NULL,0,1,'system:config:get','IconEmpty',NULL,'1','2022-08-04 11:46:11','1','2022-08-05 10:37:39'),('1555037504929808384','添加参数','1552841237894070272',4,'','{}',NULL,0,2,NULL,0,1,'system:config:add','IconEmpty',NULL,'1','2022-08-04 11:46:47','1','2022-08-05 10:37:45'),('1555037611687428096','修改参数','1552841237894070272',3,'','{}',NULL,0,2,NULL,0,1,'system:config:update','IconEmpty',NULL,'1','2022-08-04 11:47:12','1','2022-08-05 10:37:47'),('1555051931150336000','全部参数','1552841237894070272',5,'','{}',NULL,0,2,NULL,0,1,'system:config:select','IconEmpty',NULL,'1','2022-08-04 12:44:07','1','2022-08-05 10:37:43'),('1555073477185126400','服务器监控','1552843150869987328',1,'','{}',NULL,0,2,NULL,0,1,'monitor:server:list','IconEmpty',NULL,'1','2022-08-04 14:09:43','1','2022-08-05 10:38:47'),('1555074013582082048','登陆日志分页列表','1552842905746472960',1,'','{}',NULL,0,2,NULL,0,1,'logger:login:page','IconEmpty',NULL,'1','2022-08-04 14:11:51','1','2022-08-05 10:38:26'),('1555074266548944896','登陆日志详情','1552842905746472960',4,'','{}',NULL,0,2,NULL,0,1,'logger:login:get','IconEmpty',NULL,'1','2022-08-04 14:12:52','1','2022-08-05 10:38:32'),('1555074574830288896','删除登陆日志','1552842905746472960',2,'','{}',NULL,0,2,NULL,0,1,'logger:login:delete','IconEmpty',NULL,'1','2022-08-04 14:14:05','1','2022-08-05 10:38:28'),('1555075155582009344','定时任务分页列表','1552843537425432576',1,'','{}',NULL,0,2,NULL,0,1,'task:job:list','IconEmpty',NULL,'1','2022-08-04 14:16:24','1','2022-08-05 10:38:52'),('1555075325631676416','定时任务详情','1552843537425432576',2,'','{}',NULL,0,2,NULL,0,1,'task:job:query','IconEmpty',NULL,'1','2022-08-04 14:17:04','1','2022-08-05 10:38:54'),('1555075442904416256','新增定时任务','1552843537425432576',3,'','{}',NULL,0,2,NULL,0,1,'task:job:add','IconEmpty',NULL,'1','2022-08-04 14:17:32','1','2022-08-05 10:38:56'),('1555075634311479296','修改定时任务','1552843537425432576',4,'','{}',NULL,0,2,NULL,0,1,'task:job:update','IconEmpty',NULL,'1','2022-08-04 14:18:18','1','2022-08-05 10:38:58'),('1555075869700014080','定时任务状态修改','1552843537425432576',5,'','{}',NULL,0,2,NULL,0,1,'task:job:changeStatus','IconEmpty',NULL,'1','2022-08-04 14:19:14','1','2022-08-05 10:39:00'),('1555075994920960000','删除定时任务','1552843537425432576',7,'','{}',NULL,0,2,NULL,0,1,'task:job:delete','IconEmpty',NULL,'1','2022-08-04 14:19:44','1','2022-08-05 10:39:08'),('1555076352686702592','定时任务立即执行一次','1552843537425432576',6,'','{}',NULL,0,2,NULL,0,1,'task:job:run','IconEmpty',NULL,'1','2022-08-04 14:21:09','1','2022-08-05 10:39:02'),('1555078539261263872','缓存监控分页列表','1552843370894786560',1,'','{}',NULL,0,2,NULL,0,1,'monitor:cache:list','IconEmpty',NULL,'1','2022-08-04 14:29:50','1','2022-08-05 10:38:50'),('1555080390803210240','操作日志分页列表','1552842481920442368',1,'','{}',NULL,0,2,NULL,0,1,'logger:operate:page','IconEmpty',NULL,'1','2022-08-04 14:37:12','1','2022-08-05 10:38:18'),('1555080535099850752','操作日志详情','1552842481920442368',4,'','{}',NULL,0,2,NULL,0,1,'logger:operate:get','IconEmpty',NULL,'1','2022-08-04 14:37:46','1','2022-08-05 10:38:24'),('1555080649092644864','删除操作日志','1552842481920442368',2,'','{}',NULL,0,2,NULL,0,1,'logger:operate:delete','IconEmpty',NULL,'1','2022-08-04 14:38:13','1','2022-08-05 10:38:20'),('1555081159937900544','调度日志分页列表','1552843537425432576',8,'','{}',NULL,0,2,NULL,0,1,'task:logger:list','IconEmpty',NULL,'1','2022-08-04 14:40:15','1','2022-08-05 10:39:10'),('1555081328767025152','调度日志详情','1552843537425432576',11,'','{}',NULL,0,2,NULL,0,1,'task:logger:query','IconEmpty',NULL,'1','2022-08-04 14:40:55','1','2022-08-05 10:39:17'),('1555081485042597888','删除调度日志','1552843537425432576',9,'','{}',NULL,0,2,NULL,0,1,'task:logger:delete','IconEmpty',NULL,'1','2022-08-04 14:41:33','1','2022-08-05 10:39:13'),('1555081651338362880','清空调度日志','1552843537425432576',10,'','{}',NULL,0,2,NULL,0,1,'task:logger:clean','IconEmpty',NULL,'1','2022-08-04 14:42:12','1','2022-08-05 10:39:15'),('1555082477742080000','清空登录日志','1552842905746472960',3,'','{}',NULL,0,2,NULL,0,1,'logger:login:clean','IconEmpty',NULL,'1','2022-08-04 14:45:29','1','2022-08-05 10:38:30'),('1555083142040141824','清空操作日志','1552842481920442368',3,'','{}',NULL,0,2,NULL,0,1,'logger:operate:clean','IconEmpty',NULL,'1','2022-08-04 14:48:08','1','2022-08-05 10:38:22'),('1555740007753445376','重置密码','1552834825520480256',6,'','{}',NULL,0,2,NULL,0,1,'system:user:reset','IconEmpty',NULL,'1','2022-08-06 10:18:17','1','2022-08-06 10:18:34'),('1556917153800720384','分配角色','1552834825520480256',7,'','{}',NULL,0,2,NULL,0,1,'system:user:grant','IconEmpty',NULL,'1','2022-08-09 16:15:50','1','2022-08-09 17:32:07');

/*Table structure for table `system_notice` */

DROP TABLE IF EXISTS `system_notice`;

CREATE TABLE `system_notice` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '公告ID',
  `title` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '标题',
  `content` text COLLATE utf8mb4_bin COMMENT '内容',
  `type` int(1) NOT NULL COMMENT '类型（1：通知；2：公告）',
  `enabled` int(1) DEFAULT '1' COMMENT '状态（0：禁用；1：启用）',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_notice` */

insert  into `system_notice`(`id`,`title`,`content`,`type`,`enabled`,`create_by`,`create_time`,`update_by`,`update_time`) values ('1555090073207250944','温馨提醒：2022-08-04  小鹿线基础框架（Springboot+Vue版）正式发布！','<p>版本内容</p>',1,1,'1','2022-08-04 15:15:40',NULL,NULL),('1555090335569354752','维护通知：2022-08-04 小鹿线基础框架（Springboot+Vue版）将持续维护','<p>维护通知</p>',2,1,'1','2022-08-04 15:16:43','1','2022-08-10 18:11:26');

/*Table structure for table `system_oper_log` */

DROP TABLE IF EXISTS `system_oper_log`;

CREATE TABLE `system_oper_log` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '模块标题',
  `business_type` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '请求方式',
  `operator_type` tinyint(4) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '返回参数',
  `status` tinyint(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_oper_log` */

/*Table structure for table `system_post` */

DROP TABLE IF EXISTS `system_post`;

CREATE TABLE `system_post` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
  `post_name` varchar(128) CHARACTER SET utf8mb4 NOT NULL COMMENT '岗位名称',
  `post_code` varchar(32) CHARACTER SET utf8mb4 NOT NULL COMMENT '岗位编码',
  `unit_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '机构ID',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '顺序',
  `enabled` int(1) DEFAULT '0' COMMENT '是否启用（0：禁用；1：启用）',
  `remark` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_post` */

insert  into `system_post`(`id`,`post_name`,`post_code`,`unit_id`,`sort`,`enabled`,`remark`,`create_by`,`create_time`,`update_by`,`update_time`) values ('1555090700742238208','人力资源','HR',NULL,2,1,'暂无备注','1','2022-08-04 15:18:10','1','2022-08-04 15:21:02'),('1555091388415156224','项目经理','PM',NULL,1,1,'暂无备注','1','2022-08-04 15:20:54','1','2022-08-04 15:21:07'),('1555091607244578816','前端开发','FE',NULL,3,1,'暂无备注','1','2022-08-04 15:21:46',NULL,NULL);

/*Table structure for table `system_role` */

DROP TABLE IF EXISTS `system_role`;

CREATE TABLE `system_role` (
  `id` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT '主键',
  `role_name` varchar(128) CHARACTER SET utf8 NOT NULL COMMENT '角色名称',
  `role_perm` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '1' COMMENT '角色编码',
  `unit_id` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '机构ID',
  `data_privileges` int(11) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `enabled` int(1) NOT NULL DEFAULT '1' COMMENT '是否启用（0：禁用；1：启用）',
  `descript` text CHARACTER SET utf8 COMMENT '描述',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_role` */

insert  into `system_role`(`id`,`role_name`,`role_perm`,`unit_id`,`data_privileges`,`enabled`,`descript`,`create_by`,`create_time`,`update_by`,`update_time`) values ('1549284952175144960','管理员','ADMIN','1',1,1,'1','1','2022-07-19 14:48:12','1','2022-08-09 15:45:15'),('1555093603708116992','普通角色','COMMON_STAFF',NULL,1,1,'','1','2022-08-04 15:29:42','1','2022-08-06 15:11:13'),('1555355167996604416','测试角色','TEST',NULL,1,1,'','1','2022-08-05 08:49:04','1','2022-08-09 09:55:50');

/*Table structure for table `system_role_menu` */

DROP TABLE IF EXISTS `system_role_menu`;

CREATE TABLE `system_role_menu` (
  `role_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '角色ID',
  `menu_id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '菜单ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_role_menu` */

insert  into `system_role_menu`(`role_id`,`menu_id`) values ('1550288466087014400','1549234471801225216'),('1550288466087014400','1549234471801225211'),('1550290045930336256','1549234471801225216'),('1550290045930336256','1549234471801225211'),('1550289041574883328','1549234471801225216'),('1550289041574883328','1549234471801225211'),('1550310734297067520','1549234471801225216'),('1550310734297067520','1549234471801225211'),('1551493633247551488','1550392319688077312'),('1551493633247551488','1549234471801225217'),('1551493633247551488','1550412706157281280'),('1551493633247551488','1550377691646029824'),('1554268240492986368','1550392319688077312'),('1554268240492986368','1549234471801225217'),('1554268240492986368','1550412706157281280'),('1551483507807625216','1550392319688077312'),('1551483507807625216','1549234471801225217'),('1551483507807625216','1550412706157281280'),('1551483507807625216','1552834825520480256'),('1554657131532185600','1552834825520480256'),('1554657131532185600','1552837943025008640'),('1554292114211307520','1552834825520480256'),('1554292114211307520','1552837943025008640'),('1554292114211307520','1553289085710274560'),('1555093603708116992','1552832554816569344'),('1555093603708116992','1552834825520480256'),('1555093603708116992','1555021778256936960'),('1555355167996604416','1552832554816569344'),('1555355167996604416','1552834825520480256'),('1555355167996604416','1555021778256936960'),('1555355167996604416','1555022916632985600'),('1555355167996604416','1554660092320739328'),('1549284952175144960','1552832554816569344'),('1549284952175144960','1552834825520480256'),('1549284952175144960','1555021778256936960'),('1549284952175144960','1555022690253815808'),('1549284952175144960','1555023027287113728'),('1549284952175144960','1555022916632985600'),('1549284952175144960','1555022805899165696'),('1549284952175144960','1555740007753445376'),('1549284952175144960','1552837943025008640'),('1549284952175144960','1555023414698196992'),('1549284952175144960','1555024916816871424'),('1549284952175144960','1555025221142986752'),('1549284952175144960','1555025109608054784'),('1549284952175144960','1555025013080342528'),('1549284952175144960','1555025365808726016'),('1549284952175144960','1553289085710274560'),('1549284952175144960','1555025467013087232'),('1549284952175144960','1555025565591814144'),('1549284952175144960','1552838255668428800'),('1549284952175144960','1555035861660221440'),('1549284952175144960','1555036052165509120'),('1549284952175144960','1555036314888323072'),('1549284952175144960','1555036219384020992'),('1549284952175144960','1555036136961753088'),('1549284952175144960','1552840272302374912'),('1549284952175144960','1555028623730163712'),('1549284952175144960','1555028755930431488'),('1549284952175144960','1555029352754724864'),('1549284952175144960','1555029121996701696'),('1549284952175144960','1555028860968386560'),('1549284952175144960','1555029229572210688'),('1549284952175144960','1552840644655906816'),('1549284952175144960','1555026583431954432'),('1549284952175144960','1555026703904948224'),('1549284952175144960','1555027058961170432'),('1549284952175144960','1555026932775534592'),('1549284952175144960','1555026797555367936'),('1549284952175144960','1555027214834089984'),('1549284952175144960','1552840927784009728'),('1549284952175144960','1555032086073589760'),('1549284952175144960','1555032190113300480'),('1549284952175144960','1555032781178814464'),('1549284952175144960','1555032676602232832'),('1549284952175144960','1555032485929172992'),('1549284952175144960','1553289562300649472'),('1549284952175144960','1555034605604585472'),('1549284952175144960','1555035026716901376'),('1549284952175144960','1555035435468603392'),('1549284952175144960','1555035265049837568'),('1549284952175144960','1555035127086596096'),('1549284952175144960','1552841237894070272'),('1549284952175144960','1555036936706473984'),('1549284952175144960','1555037170614419456'),('1549284952175144960','1555037611687428096'),('1549284952175144960','1555037504929808384'),('1549284952175144960','1555051931150336000'),('1549284952175144960','1555037354723393536'),('1549284952175144960','1552841910891118592'),('1549284952175144960','1555029594791231488'),('1549284952175144960','1555030572403802112'),('1549284952175144960','1555031176790425600'),('1549284952175144960','1555031035136196608'),('1549284952175144960','1555030762397384704'),('1549284952175144960','1552842207126421504'),('1549284952175144960','1552842481920442368'),('1549284952175144960','1555080390803210240'),('1549284952175144960','1555080649092644864'),('1549284952175144960','1555083142040141824'),('1549284952175144960','1555080535099850752'),('1549284952175144960','1552842905746472960'),('1549284952175144960','1555074013582082048'),('1549284952175144960','1555074574830288896'),('1549284952175144960','1555082477742080000'),('1549284952175144960','1555074266548944896'),('1549284952175144960','1552831340867551232'),('1549284952175144960','1552843150869987328'),('1549284952175144960','1555073477185126400'),('1549284952175144960','1552843370894786560'),('1549284952175144960','1555078539261263872'),('1549284952175144960','1552843537425432576'),('1549284952175144960','1555075155582009344'),('1549284952175144960','1555075325631676416'),('1549284952175144960','1555075442904416256'),('1549284952175144960','1555075634311479296'),('1549284952175144960','1555075869700014080'),('1549284952175144960','1555076352686702592'),('1549284952175144960','1555075994920960000'),('1549284952175144960','1555081159937900544'),('1549284952175144960','1555081485042597888'),('1549284952175144960','1555081651338362880'),('1549284952175144960','1555081328767025152'),('1549284952175144960','1554660092320739328');

/*Table structure for table `system_unit` */

DROP TABLE IF EXISTS `system_unit`;

CREATE TABLE `system_unit` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '机构名称',
  `code` varchar(12) COLLATE utf8mb4_bin NOT NULL COMMENT '机构编码',
  `codeseq` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT '机构层级编码',
  `contact` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '联系人',
  `mobile` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机',
  `address` text COLLATE utf8mb4_bin COMMENT '地址',
  `email` varchar(90) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '邮箱',
  `web` varchar(150) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '网址',
  `parent_id` varchar(150) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上级机构ID',
  `has_children` int(1) DEFAULT NULL COMMENT '是否有下级机构',
  `system` int(1) DEFAULT NULL COMMENT '是否系统内置',
  `enabled` int(1) DEFAULT NULL COMMENT '是否启用（0：禁用；1：启用）',
  `leader_id` varchar(96) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '负责人ID',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_unit` */

insert  into `system_unit`(`id`,`name`,`code`,`codeseq`,`contact`,`mobile`,`address`,`email`,`web`,`parent_id`,`has_children`,`system`,`enabled`,`leader_id`,`create_by`,`create_time`,`update_by`,`update_time`) values ('1548118280701603840','内置组织','SXFCZ3','SXFCZ3','王俊南','15133334151','河北省张家口市涿鹿县','wang_junnan@hotmail.com','http://www.iteachyou.io','-1',1,1,1,NULL,'1','2022-07-16 09:32:15',NULL,NULL),('1548118397974343680','河北分公司','3N2Y2S','SXFCZ3.3N2Y2S',NULL,NULL,NULL,NULL,NULL,'1548118280701603840',0,0,1,NULL,'1','2022-07-16 09:32:43',NULL,NULL),('1548118517381984256','山西分公司','UUT_C7','SXFCZ3.UUT_C7',NULL,NULL,NULL,NULL,NULL,'1548118280701603840',1,0,1,NULL,'1','2022-07-16 09:33:12',NULL,NULL),('1548118609111412736','山西太原分公司','WCUGHQ','SXFCZ3.UUT_C7.WCUGHQ',NULL,NULL,NULL,NULL,NULL,'1548118517381984256',0,0,1,NULL,'1','2022-07-16 09:33:34',NULL,NULL),('1548129159916576768','山西大同分公司','EM0QDJ','SXFCZ3.UUT_C7.EM0QDJ',NULL,NULL,NULL,NULL,NULL,'1548118517381984256',0,0,1,NULL,'1','2022-07-16 10:15:29',NULL,NULL),('1550740658015297536','黑龙江分公司','C86KYO','SXFCZ3.C86KYO','','','河北省张家口市涿鹿县','hlj@xuexiluxian.cn','http://www.iteachyou.io','1548118280701603840',0,0,1,'1554658069143678976','1','2022-07-23 15:12:39','1','2022-08-03 14:23:04'),('1550746860757573632','福建分公司','Z5T8R0','SXFCZ3.Z5T8R0','','','河北省张家口市涿鹿县','aaa','http://www.iteachyou.io','1548118280701603840',1,0,1,'1554627646678167552','1','2022-07-23 15:37:18','1','2022-08-03 14:24:00');

/*Table structure for table `system_user` */

DROP TABLE IF EXISTS `system_user`;

CREATE TABLE `system_user` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '用户ID',
  `username` varchar(30) CHARACTER SET utf8mb4 NOT NULL COMMENT '登录账号',
  `password` varchar(128) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '密码',
  `real_name` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '真实姓名',
  `user_type` int(11) DEFAULT '0' COMMENT '用户类型（0：普通账号；1：超级管理员）',
  `email` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '用户邮箱',
  `phone` varchar(11) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '手机号码',
  `gender` int(11) DEFAULT '0' COMMENT '用户性别（1：男；2：女；0：未知）',
  `avatar` varchar(300) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '头像路径',
  `enabled` int(1) DEFAULT '1' COMMENT '帐号状态（0：禁用；1：正常）',
  `unit_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '机构ID',
  `del_flag` int(1) DEFAULT '0' COMMENT '是否删除（0：有效；1：删除）',
  `login_ip` varchar(50) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '最后登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `remark` text COLLATE utf8mb4_bin COMMENT '备注',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建者',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新者',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_user` */

insert  into `system_user`(`id`,`username`,`password`,`real_name`,`user_type`,`email`,`phone`,`gender`,`avatar`,`enabled`,`unit_id`,`del_flag`,`login_ip`,`login_date`,`remark`,`create_by`,`create_time`,`update_by`,`update_time`) values ('1','admin','$2a$10$UXHbi73jHfuw95ZLJBGa.eIcWqpz1CoNpA31w2jmz5U9Tg/NN1F.K','超级管理员',1,'admin@xuexiluxian.cn','13501140750',1,'/uploads/20220805/1555402905501122560.png',1,'1548118280701603840',0,'127.0.0.1','2021-06-22 23:54:40',NULL,NULL,'2018-03-16 11:33:00','1','2022-06-22 20:33:01'),('1555094716264030208','wangchao','$2a$10$x4emaKLzYuSEGUzjucRLUOJ2lbh0sHARipF9WbYCD1M8pmwBTh2Ve','wangchao',0,'2591356063@qq.com','15660139169',1,'',1,'1548118609111412736',0,'',NULL,'','1','2022-08-04 15:34:07','1','2022-08-09 19:08:51'),('1555160814095298560','asheng','$2a$10$IPkt7iuQpcq2Loqt3xL8Be0KUi93JAVD7tJXPG/h7.EWiy6fz4wKW','阿笙',0,'','13949660511',1,'',1,'1548118609111412736',0,'',NULL,'','1','2022-08-04 19:56:46','1','2022-08-09 09:24:46'),('1555355075935825920','wangjn','$2a$10$4VGXwuU4YEItR31tAIkrUOST1Vz4d1TnjiR2w5CNpO1kCTzEF0y/m','王俊南',0,'','13131343010',1,'',1,'1548118609111412736',0,'',NULL,'','1','2022-08-05 08:48:42','1','2022-08-06 11:00:35'),('1556814130474688512','ttt111','$2a$10$vSADuJyHcZnKPG.3jCO9BOlPlxWQEWr4ufy9b9ubG8i4..IcaYHWK','xx',0,'','13131343012',1,'',1,NULL,0,'',NULL,'ddd','1','2022-08-09 09:26:28',NULL,NULL);

/*Table structure for table `system_user_post` */

DROP TABLE IF EXISTS `system_user_post`;

CREATE TABLE `system_user_post` (
  `user_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `post_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_user_post` */

insert  into `system_user_post`(`user_id`,`post_id`) values ('-10','1550732935487832064'),('1555355075935825920','1555091388415156224'),('1555160814095298560','1555091607244578816'),('1556814130474688512','1555091388415156224'),('1555094716264030208','1555091607244578816');

/*Table structure for table `system_user_role` */

DROP TABLE IF EXISTS `system_user_role`;

CREATE TABLE `system_user_role` (
  `role_id` varchar(32) CHARACTER SET utf8 NOT NULL,
  `user_id` varchar(32) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `system_user_role` */

insert  into `system_user_role`(`role_id`,`user_id`) values ('1551466362356944896','-10'),('1555355167996604416','1555355075935825920'),('1555093603708116992','1555355075935825920'),('1555355167996604416','1555160814095298560'),('1555093603708116992','1555160814095298560'),('1549284952175144960','1555160814095298560'),('1555355167996604416','1555094716264030208'),('1555093603708116992','1555094716264030208'),('1555093603708116992','1556814130474688512'),('1555355167996604416','1556814130474688512');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
