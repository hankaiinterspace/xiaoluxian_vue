package cn.xuexiluxian.open.common.enums;

public enum ResultStatusCodeEnum {

    //HTTP返回值
    HTTP_SUCCESS("200","操作成功"),
    HTTP_ERROR("500","操作失败"),
    HTTP_EXCEPTION("400","服务器错误"),
    HTTP_MEDIATYPENOTSUPPORT("415","不支持的媒体类型"),
    HTTP_NOTUPDATE("304","资源未修改"),
    HTTP_NOTNOTAUTHORIZE("401","请求未授权"),
    HTTP_FORBIDDEN("403","禁止访问"),
    HTTP_NOTFOUND("404","资源不存在"),
    HTTP_METHODNOTSUPPORT("405","方法不支持"),

    USER_NOLOGIN("10001","用户未登录"),
    USERNAME_PASSWORD_ERROR("10002","用户名或密码错误"),
    USER_STATUS_DISABLE("10003","用户已禁用"),
    USER_LOGIN_ERROR("10004","登录失败"),
    USER_EXIST("10005","用户已存在"),
    USER_PASSWORD_NOTMATCH("10007","密码不匹配"),
    USER_LOGIN_EXPIRED("10008","登录超时"),
    USER_TWOPASSWORD_NOTMATCH("10009","两次密码输入不一致"),
    USER_NOT_EXIST("10010","用户不存在"),
    MOBILE_NOT_EMPTY("10011","手机号为空"),
    CAPTCHA_NOT_EMPTY("10012","验证码为空"),
    CAPTCHA_ERROR("10013", "验证码输入错误"),
    MOBILE_ERROR("10014", "手机号输入不正确"),
    PASSWORD_NOT_EXIST("10015", "密码为空"),
    MOBILE_BINDED("10016", "手机号已绑定其它用户"),
    USER_NOT_MATCH("10017", "用户不匹配"),
    SERVER_CAPTCHA_ERROR("10018", "服务器验证码错误"),
    USERNAME_NOT_SET("10021", "未设置用户名"),
    CAPTCHA_EXPIRED("10022", "验证码已过期"),

    PARAM_REQUIRE_ERROR("20001", "缺少必要参数"),
    PARAM_ILLEGAL("20002", "参数信息不合法"),
    PARAM_FILE_OVER_SIZE_ERROR("20003", "上传文件过大"),

    DATA_NO_EXIST("30001", "数据不存在"),
    BUILT_IN_DATA_NODELETE("30002", "内置数据不允许删除"),
    DATA_EXIST("30003", "数据已存在"),
    DATA_HAS_CHILDREN("30004", "数据存在下级资源"),
    DATA_HAS_RESOURCES("30005", "数据存在引用资源"),


    TOKEN_NOT_EXIST("50001", "Authorization Token不存在"),
    TOKEN_INVALID("50002", "Authorization Token无效"),
    TOKEN_EXPIRED("50003", "Authorization Token已过期"),
    LOGIN_PARAM_REQUIRE_ERROR("50004", "开启了登录验证码校验，缺少参数[key,captcha]"),
    ;

    private String code;
    private String desc;

    private ResultStatusCodeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
