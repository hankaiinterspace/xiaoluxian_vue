package cn.xuexiluxian.open.common.enums;

/**
 * 短信验证码类型
 */
@SuppressWarnings("AlibabaEnumConstantsMustHaveComment")
public enum SmsCodeEnum {
    LOGIN(1, "MOBILE_CAPTCHA:LOGIN_CODE:", "SMS_172223479","登录验证码{0},非本人操作可以忽略！[5分钟有效]", "登录验证码"),
    REGISTER(2, "MOBILE_CAPTCHA:REGISTER_CODE:", "SMS_*********","注册验证码{0},非本人操作可以忽略！[5分钟有效]", "注册验证码"),
    FORGET_PWD(3, "MOBILE_CAPTCHA:FORGET_PWD_CODE:", "SMS_*********","验证码{0}，您正在找回密码，打死不要告诉别人哦！[5分钟有效]", "忘记密码验证码"),
    CHANGE_TEL_OLD(4, "MOBILE_CAPTCHA:CHANGE_TEL_CODE:", "SMS_*********","验证码{0}，您正在更换手机号，非本人操作可以忽略！[5分钟有效]", "更换手机号-原手机短信"),
    CHANGE_TEL_NEW(5, "MOBILE_CAPTCHA:CHANGE_TEL_NEW_CODE:", "SMS_*********","验证码{0}，您正在更换手机号，打死不要告诉别人哦！[5分钟有效]", "更换手机号-新手机短信"),
    ;


    /**
     * 发送短信时指定的type
     */
    private Integer type;
    /**
     * 存放Redis缓存的key前缀
     */
    private String key;
    /**
     * 短信模板编号
     */
    private String templateCode;
    /**
     * 短信模板内容
     */
    private String message;
    /**
     * 描述
     */
    private String desc;

    SmsCodeEnum(Integer type, String key, String templateCode, String message, String desc) {
        this.type = type;
        this.key = key;
        this.templateCode = templateCode;
        this.message = message;
        this.desc = desc;
    }

    public static SmsCodeEnum getSmsCodeEnum(Integer type) {
        for (SmsCodeEnum codeEnum : SmsCodeEnum.values()) {
            if (codeEnum.getType().equals(type)) {
                return codeEnum;
            }
        }
        return null;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }
}
