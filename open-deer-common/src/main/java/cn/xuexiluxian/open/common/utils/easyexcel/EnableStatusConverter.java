package cn.xuexiluxian.open.common.utils.easyexcel;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;

/**
 * @Description 启用禁用状态转换
 * @Author 王俊南
 * @Date 2022/6/1 10:55 下午
 **/
public class EnableStatusConverter implements Converter<Integer> {
    @Override
    public Class<?> supportJavaTypeKey() {
        return Integer.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    /**
     * 这里读的时候会调用
     *
     * @param context
     * @return
     */
    @Override
    public Integer convertToJavaData(ReadConverterContext<?> context) {
        if("禁用".equals(context.getReadCellData().getStringValue())){
            return  1;
        }
        return 0;
    }

    /**
     * 这里是写的时候会调用 不用管
     *
     * @return
     */
    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Integer> context) {
        if (context.getValue() == 1) {
            return new WriteCellData("禁用");
        }
        return new WriteCellData("启用");
    }
}
