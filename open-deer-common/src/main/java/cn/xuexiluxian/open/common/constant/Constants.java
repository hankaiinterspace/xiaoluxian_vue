package cn.xuexiluxian.open.common.constant;

public class Constants {
    public static final Object CRT_VERSION = "1.0.1";

    public static final String USERNAME_PASSWORD_LOGIN_URL = "/u/loginByJson";
    public static final String MOBILE_LOGIN_URL = "/u/loginByMobile";

    public static final String UTF8 = "UTF-8";

    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    public static final String SUPER_ADMIN_ROLE = "ROLE_SUPER_ADMIN";

    /**
     * 菜单类型（目录）
     */
    public static final Integer TYPE_DIR = 0;

    /**
     * 菜单类型（菜单）
     */
    public static final Integer TYPE_MENU = 1;

    /**
     * 菜单类型（按钮）
     */
    public static final Integer TYPE_BUTTON = 2;

    /**
     * 通用成功标识
     */
    public static final Integer SUCCESS = 0;

    /**
     * 通用失败标识
     */
    public static final Integer FAIL = 1;
    /**
     * Layout组件标识
     */
    public static final String LAYOUT = "layoutHeaderAside";
    /**
     * ParentView组件标识
     */
    public final static String PARENT_VIEW = "ParentView";

    /**
     * InnerLink组件标识
     */
    public final static String INNER_LINK = "InnerLink";
    /**
     * 资源映射路径 前缀
     */
    public final static String RESOURCE_PREFIX = "/uploads";

    /**
     * RMI 远程方法调用
     */
    public static final String LOOKUP_RMI = "rmi:";

    /**
     * LDAP 远程方法调用
     */
    public static final String LOOKUP_LDAP = "ldap:";

    /**
     * LDAPS 远程方法调用
     */
    public static final String LOOKUP_LDAPS = "ldaps:";

    /**
     * 定时任务白名单配置（仅允许访问的包名，如其他需要可以自行添加）
     */
    public static final String[] JOB_WHITELIST_STR = { "cn.xuexiluxian.open" };

    /**
     * 定时任务违规的字符
     */
    public static final String[] JOB_ERROR_STR = { "java.net.URL", "javax.naming.InitialContext", "org.yaml.snakeyaml",
            "org.springframework", "org.apache" };

    /**
     * 默认允许导出的excel最大行数
     */
    public static final int EXPORT_MAX_ROWS = 1000000;

    /**
     * 顶级机构ID
     */
    public static final String UNIT_ROOT_ID = "-1";

    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";
}
