package cn.xuexiluxian.open.common.enums;

/**
 * @Description 字典表的字段名称
 * @Author 王俊南
 * @Date 2022/6/2 1:58 下午
 **/
public enum DictFiledEnum {
    /**
     * 字典标签
     */
    LABLE,

    /**
     * 字典值
     */
    VALUE,
}
