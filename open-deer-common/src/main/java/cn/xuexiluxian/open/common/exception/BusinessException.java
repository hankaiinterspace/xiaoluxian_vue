package cn.xuexiluxian.open.common.exception;


import cn.xuexiluxian.open.common.enums.ResultStatusCodeEnum;

/**
 * @Description 业务异常
 * @Author 王俊南
 * @Date 2021/7/3 5:09 下午
 **/
public class BusinessException extends RuntimeException{

        private String code;

    public BusinessException(String message) {
        super(message);
        this.code = "500";
    }

    public BusinessException(String code, String message) {
        super(message);
        this.code = code;
    }

    public BusinessException(ResultStatusCodeEnum stateCodeEnum) {
        super(stateCodeEnum.getDesc());
        this.code = stateCodeEnum.getCode();
    }

    public String getCode() {
        return code;
    }
}
