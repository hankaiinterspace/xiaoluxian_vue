package cn.xuexiluxian.open.common.enums;


/**
 * @Description
 * @Author 王俊南
 * @Date 2022/5/22 7:53 下午
 **/
public enum YesOrNoEnum {
    /**
     * 1-是
     */
    YES("1", 1, true, "是"),
    /**
     * 0-否
     */
    NO("0", 0, false, "否"),
    ;

    String code;
    int type;
    boolean bool;
    String desc;

    YesOrNoEnum(String code, int type, boolean bool, String desc) {
        this.code = code;
        this.type = type;
        this.bool = bool;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean getBool() {
        return bool;
    }

    public void setBool(boolean bool) {
        this.bool = bool;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}