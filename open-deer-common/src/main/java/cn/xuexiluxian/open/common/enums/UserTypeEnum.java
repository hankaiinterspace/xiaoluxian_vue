package cn.xuexiluxian.open.common.enums;

/**
 * 用户账号类型
 */
public enum UserTypeEnum {
	SUPER_ADMIN(1, "超级管理员"),
	OTHER(0, "普通账号"),
	;

	private Integer code;
	private String desc;

	UserTypeEnum(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public static UserTypeEnum getLogType(Integer code){
		for (UserTypeEnum item : UserTypeEnum.values()) {
			if(item.getCode().equals(code)){
				return item;
			}
		}
		return null;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
