package cn.xuexiluxian.open.common.utils.easyexcel;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.data.WriteCellData;

/**
 * @Description
 * @Author 王俊南
 * @Date 2022/6/1 10:55 下午
 **/
public class SexConverter implements Converter<Integer> {
    @Override
    public Class<?> supportJavaTypeKey() {
        return Integer.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    /**
     * 这里读的时候会调用
     *
     * @param context
     * @return
     */
    @Override
    public Integer convertToJavaData(ReadConverterContext<?> context) {
        if("男".equals(context.getReadCellData().getStringValue())){
            return  1;
        }else if("女".equals(context.getReadCellData().getStringValue())){
            return  0;
        }
        return 2;
    }

    /**
     * 这里是写的时候会调用 不用管
     *
     * @return
     */
    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Integer> context) {
        if (context.getValue() == 1) {
            return new WriteCellData("男");
        } else if (context.getValue() == 0) {
            return new WriteCellData("女");
        }
        return new WriteCellData("未知");

    }
}
