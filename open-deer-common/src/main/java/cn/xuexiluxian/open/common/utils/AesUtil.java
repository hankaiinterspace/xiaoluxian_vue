package cn.xuexiluxian.open.common.utils;

import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;

public class AesUtil {

    public static final String key = "bGvnMc62sh5RV6zP";
    public static final String iv = "1eZ43DLcYtV2xb3Y";

    public static String encrypt(String string){
        AES aes = new AES(Mode.CBC, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());
        return aes.encryptHex(string);
    }

    public static String decrypt(String string){
        AES aes = new AES(Mode.CBC, Padding.PKCS5Padding, key.getBytes(), iv.getBytes());
        return aes.decryptStr(string);
    }
}
