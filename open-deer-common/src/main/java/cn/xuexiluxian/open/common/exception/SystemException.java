package cn.xuexiluxian.open.common.exception;


import cn.xuexiluxian.open.common.enums.ResultStatusCodeEnum;

/**
 * 系统异常
 *
 */
public class SystemException extends RuntimeException {

    private ResultStatusCodeEnum codeEnum;

    public SystemException(String message) {
        this(ResultStatusCodeEnum.HTTP_ERROR, message);
    }

    public SystemException(ResultStatusCodeEnum codeEnum) {
        this(codeEnum, "");
    }

    public SystemException(ResultStatusCodeEnum codeEnum, String message) {
        super(message);
        this.codeEnum = codeEnum;
    }

    public ResultStatusCodeEnum getCodeEnum() {
        return this.codeEnum;
    }
}
