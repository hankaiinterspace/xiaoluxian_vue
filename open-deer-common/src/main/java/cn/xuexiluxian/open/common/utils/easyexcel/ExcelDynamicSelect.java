package cn.xuexiluxian.open.common.utils.easyexcel;


/**
 * @Description EasyExcel动态下拉框父接口
 * @Author 王俊南
 * @Date 2022/6/2 11:54 上午
 **/
public interface ExcelDynamicSelect {
    /**
     * 获取动态生成的下拉框可选数据
     * @return 动态生成的下拉框可选数据
     */
    String[] getSource(ExcelSelected excelSelected);
}
