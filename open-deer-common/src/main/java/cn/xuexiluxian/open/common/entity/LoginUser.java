package cn.xuexiluxian.open.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.beans.Transient;
import java.util.Collection;
import java.util.Set;

/**
 * @Description 系统用户
 * @Author 王俊南
 * @Date 2021/6/29 9:50 下午
 **/
@Data
public class LoginUser implements UserDetails {
    private String id;
    private String username;
    @JsonIgnore
    private String password;
    private String realName;
    private Integer userType;
    private String email;
    private String phone;
    private Integer gender;
    private String avatar;
    private String unitId;
    private Integer enabled;
    private Byte delFlag;
    private String remark;

    private Set<String> roles;
    private Set<String> permissions;
    private Object unit;

    @Transient
    public boolean isAdmin() {
        if(userType != null) {
            return userType == 1;
        }
        return false;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled == 1;
    }
}
