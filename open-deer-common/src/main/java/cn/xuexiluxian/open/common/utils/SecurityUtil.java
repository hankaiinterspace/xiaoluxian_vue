package cn.xuexiluxian.open.common.utils;


import cn.xuexiluxian.open.common.entity.LoginUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 登录相关工具类
 */
public class SecurityUtil {
    /**
     * 用户ID
     **/
    public static String getUserId() {
        LoginUser user = getCurrentUser();
        return user == null ? null : user.getId();
    }

    /**
     * 获取部门ID
     **/
    public static String getUnitId() {
        LoginUser user = getCurrentUser();
        return user == null ? null : user.getUnitId();
    }

    /**
     * 获取用户账户
     **/
    public static String getUsername() {
        LoginUser user = getCurrentUser();
        return user == null ? null : user.getUsername();
    }

    /**
     * 获取用户
     *
     * @return*/
    public static LoginUser getCurrentUser() {
        LoginUser loginUser = (LoginUser) getAuthentication().getPrincipal();
        return loginUser;
    }

    /**
     * 获取Authentication
     */
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
}
