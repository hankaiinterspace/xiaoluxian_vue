/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.xuexiluxian.open.common.constant;

/**
 * 缓存的key 常量
 */
public class CacheConstants {

	/**
	 * 验证码前缀
	 */
	public static final String IMAGE_CAPTCHA_PREFIX = "IMAGE_CAPTCHA:";

	/**
	 * 字典信息缓存
	 */
	public static final String DICT_DETAILS = "dict_details";

	/**
	 * 参数缓存
	 */
	public static final String CONFIG_DETAILS = "config_details";

}
