<p align="center">
	<img alt="logo" src="https://openoss.xuexiluxian.cn/logo.png" width="350" title="鹿线开源">
</p>

<p align="center">

[![OSCS Status](https://www.oscs1024.com/platform/badge/opendeer/xiaoluxian_vue.git.svg?size=small)](https://www.murphysec.com/dr/pPaCNlwJKfBSLGw4Jq)
[![Apache-2.0](https://shields.io/badge/license-MIT-green)](https://github.com/murphysecurity/murphysec/blob/master/LICENSE)
[![Star 1000+](https://img.shields.io/badge/stars-%E2%98%85%E2%98%85%E2%98%85%E2%98%85%E2%98%86-brightgreen)](https://gitee.com/isoftforce/dreamer_cms/stargazers)

</p>

# 小鹿线基础权限框架（Springboot+Vue版）
当前版本：1.0.1

1. 鹿线开源官网：http://open.xuexiluxian.cn（建设中）
2. 演示网址：http://demo.open.xuexiluxian.cn
   - 演示账号：admin
   - 演示密码：123456

QQ群交流：
- ① 530344089

Open-Deer（小鹿线基础权限框架）史上最精简的基础权限框架系统，完全开源、完全免费。公开解决了快速搭建展示型网站（如：企业官网、技术博客、信息门户等）的框架体系，是电子政务、电信综合门户、企业信息门户、知识管理平台、电子商务平台的基础性软件系统。可以帮助政府、企业或组织灵活、准确、高效、智能地管理信息内容，实现信息的采集、加工、审核、发布、存储、检索、统计、分析、 反馈等整个信息生命周期的管理。采用时下最流行的Springboot+Vue3.0框架搭建开发，具有灵活小巧，配置简单，快速开发等特点。主要解决公司搭建网站成本高、投入大、周期长等问题，也可作为初创公司很好的基础技术框架。

# 内置功能

1. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2. 机构管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3. 岗位管理：配置系统用户所属担任职务。
4. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5. 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6. 字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7. 参数管理：对系统动态配置常用参数。
8. 通知公告：系统通知公告信息发布维护。
9. 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
12. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
13. 缓存监控：对系统的缓存查询，删除、清空等操作。

# 模块说明
* open-deer-admin：管理系统控制层
* open-deer-common：系统公共类库
* open-deer-dependencies：统一依赖模块（第三方Jar）
* open-deer-framework：管理系统安全、配置项等
* open-deer-quertz：定时任务模块
* open-deer-system：管理系统业务逻辑层（Service、Dao等）
* open-deer-web：前端Vue3源码

# 技术框架
### 后端技术框架
* 数据库：Mysql 5.7.30
* 核心框架：Spring Boot 2
* 安全框架：Spring Security 5.6.3
* 持久框架：MyBatis-Plus 3.5.1
* 日志管理：Logback
* 工具包：Hutool 5.8.0
* 其它框架：Lombok、Qrartz、
### 前端技术框架
* 核心框架：Vue3、Vue-Router、Pinia
* UI框架：Element-Plus
* Ajax框架：Axios
* 富文本：Wangeditor
* 其它框架：Crypto.js、Echarts

# 在线体验

1. 鹿线开源官网：http://open.xuexiluxian.cn（建设中）
2. 演示网址：http://demo.open.xuexiluxian.cn
    - 演示账号：admin
    - 演示密码：123456

# 参与贡献

1. 王俊南、顾弦笙、Wgz_57
2. 特别鸣谢《小鹿线》
   - http://www.xuexiluxian.cn
3. 特别鸣谢《I Teach You，我教你！》
   - http://www.iteachyou.cc
4. 项目中部分功能参考《RuoYi，若依》
   - http://www.ruoyi.vip/

# 系统美图


<table>
   <tr>
      <td><img src="https://openoss.xuexiluxian.cn/20220803140919.png"/></td>
      <td><img src="https://openoss.xuexiluxian.cn/20220803170626.png"/></td>
   </tr>
   <tr>
      <td><img src="https://openoss.xuexiluxian.cn/20220803170655.png"/></td>
      <td><img src="https://openoss.xuexiluxian.cn/20220803170704.png"/></td>
   </tr>
   <tr>
      <td><img src="https://openoss.xuexiluxian.cn/20220803170718.png"/></td>
      <td><img src="https://openoss.xuexiluxian.cn/20220803170748.png"/></td>
   </tr>
   <tr>
      <td><img src="https://openoss.xuexiluxian.cn/20220803170757.png"/></td>
      <td><img src="https://openoss.xuexiluxian.cn/20220803170813.png"/></td>
   </tr>
</table>
