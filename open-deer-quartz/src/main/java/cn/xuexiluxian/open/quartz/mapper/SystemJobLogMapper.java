package cn.xuexiluxian.open.quartz.mapper;

import cn.xuexiluxian.open.quartz.entity.SystemJobLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;

/**
 * 调度任务日志信息 数据层
 */
public interface SystemJobLogMapper extends BaseMapper<SystemJobLog> {

    /**
     * 清空任务日志
     */
    @Delete("truncate table system_job_log")
    void cleanJobLog();
}
