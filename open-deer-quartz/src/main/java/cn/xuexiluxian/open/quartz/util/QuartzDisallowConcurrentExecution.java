package cn.xuexiluxian.open.quartz.util;

import cn.xuexiluxian.open.quartz.entity.SystemJob;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;

/**
 * 定时任务处理（禁止并发执行）
 */
@DisallowConcurrentExecution
public class QuartzDisallowConcurrentExecution extends AbstractQuartzJob {
    @Override
    protected void doExecute(JobExecutionContext context, SystemJob sysJob) throws Exception {
        JobInvokeUtil.invokeMethod(sysJob);
    }
}
