package cn.xuexiluxian.open.quartz.service.impl;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.quartz.constants.ScheduleConstants;
import cn.xuexiluxian.open.quartz.entity.SystemJob;
import cn.xuexiluxian.open.quartz.mapper.SystemJobMapper;
import cn.xuexiluxian.open.quartz.service.SystemJobService;
import cn.xuexiluxian.open.quartz.util.CronUtils;
import cn.xuexiluxian.open.quartz.util.ScheduleUtils;
import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * 定时任务调度信息 服务层
 */
@Service
public class SystemJobServiceImpl extends ServiceImpl<SystemJobMapper, SystemJob> implements SystemJobService {
    @Autowired
    private Scheduler scheduler;

    /**
     * 项目启动时，初始化定时器 主要是防止手动修改数据库导致未同步到定时任务处理（注：不能手动修改数据库ID和任务组名，否则会导致脏数据）
     */
    @PostConstruct
    public void init() throws SchedulerException {
        scheduler.clear();
        List<SystemJob> jobList = list();
        for (SystemJob job : jobList) {
            ScheduleUtils.createScheduleJob(scheduler, job);
        }
    }

    @Override
    public Page<SystemJob> pageList(Page page, SystemJob bean) {
        LambdaQueryWrapper<SystemJob> queryWrapper = Wrappers.<SystemJob>lambdaQuery()
                .like(StrUtil.isNotBlank(bean.getJobName()), SystemJob::getJobName, bean.getJobName())
                .eq(StrUtil.isNotBlank(bean.getJobGroup()), SystemJob::getJobGroup, bean.getJobGroup())
                .eq(bean.getConcurrent() != null, SystemJob::getConcurrent, bean.getConcurrent())
                .eq(bean.getStatus() != null, SystemJob::getStatus, bean.getStatus());
        return page(page, queryWrapper);
    }

    /**
     * 暂停任务
     *
     * @param job 调度信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void pauseJob(SystemJob job) throws SchedulerException {
        String jobId = job.getId();
        String jobGroup = job.getJobGroup();
        job.setStatus(ScheduleConstants.Status.PAUSE.getValue());
        if (updateById(job)) {
            scheduler.pauseJob(ScheduleUtils.getJobKey(jobId, jobGroup));
        }
    }

    /**
     * 恢复任务
     *
     * @param job 调度信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void resumeJob(SystemJob job) throws SchedulerException {
        String jobId = job.getId();
        String jobGroup = job.getJobGroup();
        job.setStatus(ScheduleConstants.Status.NORMAL.getValue());
        if (updateById(job)) {
            scheduler.resumeJob(ScheduleUtils.getJobKey(jobId, jobGroup));
        }
    }

    /**
     * 删除任务后，所对应的trigger也将被删除
     *
     * @param job 调度信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteJob(SystemJob job) throws SchedulerException {
        String jobId = job.getId();
        String jobGroup = job.getJobGroup();
        if (removeById(jobId)) {
            scheduler.deleteJob(ScheduleUtils.getJobKey(jobId, jobGroup));
        }
    }

    /**
     * 批量删除调度信息
     *
     * @param ids 需要删除的任务ID
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteJobByIds(List<String> ids) throws SchedulerException {
        for (String jobId : ids) {
            SystemJob job = getById(jobId);
            deleteJob(job);
        }
    }

    /**
     * 任务调度状态修改
     *
     * @param job 调度信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void changeStatus(SystemJob job) throws SchedulerException {
        Integer status = job.getStatus();
        if (ScheduleConstants.Status.NORMAL.getValue().equals(status)) {
            resumeJob(job);
        } else if (ScheduleConstants.Status.PAUSE.getValue().equals(status)) {
            pauseJob(job);
        }
    }

    /**
     * 立即运行任务
     *
     * @param id 任务id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void run(String id) throws SchedulerException {
        SystemJob sysJob = getById(id);
        // 参数
        JobDataMap dataMap = new JobDataMap();
        dataMap.put(ScheduleConstants.TASK_PROPERTIES, sysJob);
        scheduler.triggerJob(ScheduleUtils.getJobKey(sysJob.getId(), sysJob.getJobGroup()), dataMap);
    }

    /**
     * 新增任务
     *
     * @param job 调度信息 调度信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertJob(SystemJob job) throws SchedulerException {
        if (save(job)) {
            ScheduleUtils.createScheduleJob(scheduler, job);
        }
    }

    /**
     * 更新任务的时间表达式
     *
     * @param job 调度信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateJob(SystemJob job) throws SchedulerException {
        SystemJob properties = getById(job.getId());
        if (updateById(job)) {
            updateSchedulerJob(job, properties.getJobGroup());
        }
    }

    /**
     * 更新任务
     *
     * @param job      任务对象
     * @param jobGroup 任务组名
     */
    public void updateSchedulerJob(SystemJob job, String jobGroup) throws SchedulerException {
        String jobId = job.getId();
        // 判断是否存在
        JobKey jobKey = ScheduleUtils.getJobKey(jobId, jobGroup);
        if (scheduler.checkExists(jobKey)) {
            // 防止创建时存在数据问题 先移除，然后在执行创建操作
            scheduler.deleteJob(jobKey);
        }
        ScheduleUtils.createScheduleJob(scheduler, job);
    }

    /**
     * 校验cron表达式是否有效
     *
     * @param cronExpression 表达式
     * @return 结果
     */
    @Override
    public boolean checkCronExpressionIsValid(String cronExpression) {
        return CronUtils.isValid(cronExpression);
    }
}
