package cn.xuexiluxian.open.quartz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xuexiluxian.open.quartz.entity.SystemJob;

public interface SystemJobMapper extends BaseMapper<SystemJob> {
}
