package cn.xuexiluxian.open.quartz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.quartz.entity.SystemJobLog;
import cn.xuexiluxian.open.quartz.entity.req.JobLogREQ;

/**
 * 定时任务调度日志信息信息 服务层
 */
public interface SysJobLogService extends IService<SystemJobLog> {

    /**
     * 清空任务日志
     */
    void cleanJobLog();

    Page pageList(Page page, JobLogREQ bean);
}
