package cn.xuexiluxian.open.quartz.entity.req;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description
 * @Author 王俊南
 * @Date 2022/5/24 10:38 上午
 **/
@Data
public class JobLogREQ implements Serializable {
    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 任务组名
     */
    private String jobGroup;


    /**
     * 执行状态（0正常 1失败）
     */
    private Integer status;

    /**
     * 时间范围开始
     */
    private Date beginTime;

    /**
     * 时间范围结束
     */
    private Date endTime;
}
