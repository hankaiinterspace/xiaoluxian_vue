package cn.xuexiluxian.open.quartz.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.quartz.entity.SystemJob;
import org.quartz.SchedulerException;

import java.util.List;

/**
 * 定时任务调度信息信息 服务层
 */
public interface SystemJobService extends IService<SystemJob> {
    Page<SystemJob> pageList(Page page, SystemJob bean);


    /**
     * 暂停任务
     *
     * @param job 调度信息
     * @return 结果
     */
    void pauseJob(SystemJob job) throws SchedulerException;

    /**
     * 恢复任务
     *
     * @param job 调度信息
     * @return 结果
     */
    void resumeJob(SystemJob job) throws SchedulerException;

    /**
     * 删除任务后，所对应的trigger也将被删除
     *
     * @param job 调度信息
     * @return 结果
     */
    void deleteJob(SystemJob job) throws SchedulerException;

    /**
     * 批量删除调度信息
     *
     * @param ids) 需要删除的任务ID
     * @return 结果
     */
    void deleteJobByIds(List<String> ids) throws SchedulerException;

    /**
     * 任务调度状态修改
     *
     * @param job 调度信息
     * @return 结果
     */
    void changeStatus(SystemJob job) throws SchedulerException;

    /**
     * 立即运行任务
     *
     * @param id 任务id
     * @return 结果
     */
    void run(String id) throws SchedulerException;

    /**
     * 新增任务
     *
     * @param job 调度信息
     * @return 结果
     */
    void insertJob(SystemJob job) throws SchedulerException;

    /**
     * 更新任务
     *
     * @param job 调度信息
     * @return 结果
     */
    void updateJob(SystemJob job) throws SchedulerException;

    /**
     * 校验cron表达式是否有效
     *
     * @param cronExpression 表达式
     * @return 结果
     */
    boolean checkCronExpressionIsValid(String cronExpression);
}
