package cn.xuexiluxian.open.quartz.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.quartz.entity.SystemJobLog;
import cn.xuexiluxian.open.quartz.entity.req.JobLogREQ;
import cn.xuexiluxian.open.quartz.mapper.SystemJobLogMapper;
import cn.xuexiluxian.open.quartz.service.SysJobLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 定时任务调度日志信息
 */
@Service
public class SystemJobLogServiceImpl extends ServiceImpl<SystemJobLogMapper, SystemJobLog> implements SysJobLogService {
    @Autowired
    private SystemJobLogMapper systemJobLogMapper;

    /**
     * 清空任务日志
     */
    @Override
    public void cleanJobLog() {
        systemJobLogMapper.cleanJobLog();
    }

    @Override
    public Page pageList(Page page, JobLogREQ bean) {
        LambdaQueryWrapper<SystemJobLog> queryWrapper = Wrappers.<SystemJobLog>lambdaQuery()
                .like(StrUtil.isNotBlank(bean.getJobName()), SystemJobLog::getJobName, bean.getJobName())
                .eq(StrUtil.isNotBlank(bean.getJobGroup()), SystemJobLog::getJobGroup, bean.getJobGroup())
                .eq(bean.getStatus() != null, SystemJobLog::getStatus, bean.getStatus())
                .ge(bean.getBeginTime() != null, SystemJobLog::getCreateTime, bean.getBeginTime())
                .lt(bean.getEndTime() != null, SystemJobLog::getCreateTime, bean.getEndTime());
        return page(page, queryWrapper);
    }
}
