package cn.xuexiluxian.open.system.model.req;

import lombok.Data;

@Data
public class SystemRoleREQ {
    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色编码
     */
    private String rolePerm;

    /**
     * 是否启用（0：禁用；1：启用）
     */
    private Integer enabled;
}
