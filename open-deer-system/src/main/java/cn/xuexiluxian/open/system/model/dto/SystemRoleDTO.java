package cn.xuexiluxian.open.system.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class SystemRoleDTO {
    /**
     * 主键
     */
    private String id;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色编码
     */
    private String rolePerm;

    /**
     * 机构ID
     */
    private String unitId;

    /**
     * 数据权限
     */
    private Integer dataPrivileges;

    /**
     * 是否启用（0：禁用；1：启用）
     */
    private Integer enabled;

    /**
     * 描述
     */
    private String descript;

    private List<String> permissionIds;
}
