package cn.xuexiluxian.open.system.model.req;

import lombok.Data;

@Data
public class SystemNoticeREQ {
    /**
     * 标题
     */
    private String title;

    /**
     * 类型（1：通知；2：公告）
     */
    private Integer type;

    /**
     * 状态（0：禁用；1：启用）
     */
    private Integer enabled;
}
