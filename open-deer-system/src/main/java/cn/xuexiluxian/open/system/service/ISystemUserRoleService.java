package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.entity.SystemUser;
import cn.xuexiluxian.open.system.model.entity.SystemUserRole;
import cn.xuexiluxian.open.system.model.req.SystemUserRoleREQ;

import java.util.List;

public interface ISystemUserRoleService extends IService<SystemUserRole> {
    Page<SystemUser> selectListByRoleId(Page page, SystemUserRoleREQ params);

    boolean cancel(List<SystemUserRole> systemUserRoles);

    List<String> selectCheckedByUserId(String userId);
}
