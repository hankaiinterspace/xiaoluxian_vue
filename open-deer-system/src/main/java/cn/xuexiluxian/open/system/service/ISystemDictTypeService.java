package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.entity.SystemDictType;
import cn.xuexiluxian.open.system.model.req.SystemDictTypeREQ;

public interface ISystemDictTypeService extends IService<SystemDictType> {
    Page<SystemDictType> pageList(Page page, SystemDictTypeREQ params);
}
