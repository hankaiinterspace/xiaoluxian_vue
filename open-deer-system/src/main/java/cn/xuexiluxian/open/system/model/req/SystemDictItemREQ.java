package cn.xuexiluxian.open.system.model.req;

import lombok.Data;

@Data
public class SystemDictItemREQ {
    /**
     * 字典类型ID
     */
    private String dictType;

    /**
     * 键
     */
    private String key;

    /**
     * 值
     */
    private String value;
}
