package cn.xuexiluxian.open.system.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class SystemDictTypeDTO {
    @TableId(type = IdType.AUTO)
    private String id;

    /**
     * 字典类型名称
     */
    private String name;

    /**
     * 分类
     */
    private String type;

    /**
     * 描述
     */
    private String remarks;
}
