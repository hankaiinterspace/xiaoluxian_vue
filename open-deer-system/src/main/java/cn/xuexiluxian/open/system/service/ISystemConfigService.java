package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.entity.SystemConfig;
import cn.xuexiluxian.open.system.model.req.SystemConfigREQ;

public interface ISystemConfigService extends IService<SystemConfig> {
    Page<SystemConfig> pageList(Page page, SystemConfigREQ params);

    SystemConfig selectOneByKey(String key);
}
