package cn.xuexiluxian.open.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.system.mapper.SystemUserPostMapper;
import cn.xuexiluxian.open.system.model.entity.SystemUserPost;
import cn.xuexiluxian.open.system.service.ISystemUserPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemUserPostService extends ServiceImpl<SystemUserPostMapper, SystemUserPost> implements ISystemUserPostService {
    @Autowired
    private SystemUserPostMapper systemUserPostMapper;

    @Override
    public List<String> selectCheckedByUserId(String userId) {
        return systemUserPostMapper.selectCheckedByUserId(userId);
    }
}
