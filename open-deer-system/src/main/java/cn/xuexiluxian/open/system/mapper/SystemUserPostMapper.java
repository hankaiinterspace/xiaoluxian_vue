package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xuexiluxian.open.system.model.entity.SystemUserPost;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * SystemUserPostMapper继承基类
 */
public interface SystemUserPostMapper extends BaseMapper<SystemUserPost> {
    @Select("select post_id from system_user_post where user_id = #{userId}")
    List<String> selectCheckedByUserId(String userId);
}