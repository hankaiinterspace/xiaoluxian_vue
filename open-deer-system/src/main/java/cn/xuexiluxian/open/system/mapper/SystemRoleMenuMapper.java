package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xuexiluxian.open.system.model.entity.SystemRoleMenu;

import java.util.List;

/**
 * SystemRoleMenuMapper继承基类
 */
public interface SystemRoleMenuMapper extends BaseMapper<SystemRoleMenu> {
    List<String> selectCheckedByRoleId(String roleId);
}