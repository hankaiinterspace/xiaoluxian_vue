package cn.xuexiluxian.open.system.model.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 
 * 
 */
@Data
@TableName(value = "system_config")
public class SystemConfig implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private String id;

    /**
     * 键
     */
    private String k;

    /**
     * 值
     */
    private String v;

    /**
     * 类型（1：系统内置；2：自定义）
     */
    private Integer type;

    /**
     * 是否启用（0：禁用；1：启用）
     */
    private Integer enabled;

    private static final long serialVersionUID = 1L;
}