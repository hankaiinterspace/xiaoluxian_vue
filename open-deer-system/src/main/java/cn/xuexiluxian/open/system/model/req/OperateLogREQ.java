package cn.xuexiluxian.open.system.model.req;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description
 * @Author 王俊南
 * @Date 2022/5/20 10:59 上午
 **/
@Data
public class OperateLogREQ implements Serializable {
    /**
     * 操作模块
     */
    private String title;

    /**
     * 业务类型
     *  其它：OTHER；查询：SELECT,新增：INSERT；修改：UPDATE；删除：DELETE；授权：GRANT；导出：EXPORT；导入：IMPORT；强退：FORCE；清空数据：CLEAN
     */
    private String businessType;

    /**
     * 操作人员
     */
    private String operName;

    /**
     * 操作状态（0正常 1异常）
     */
    private Integer status;

    /**
     * 操作时间范围开始
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;

    /**
     * 操作时间范围结束
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
}
