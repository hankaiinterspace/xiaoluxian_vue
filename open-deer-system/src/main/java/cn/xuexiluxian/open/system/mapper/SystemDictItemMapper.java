package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.system.model.entity.SystemDictItem;
import cn.xuexiluxian.open.system.model.req.SystemDictItemREQ;

import java.util.List;

/**
 * SystemDictItemMapper继承基类
 */
public interface SystemDictItemMapper extends BaseMapper<SystemDictItem> {
    Page<SystemDictItem> page(Page page, SystemDictItemREQ params);

    List<SystemDictItem> selectListByDictType(String dictType);
}