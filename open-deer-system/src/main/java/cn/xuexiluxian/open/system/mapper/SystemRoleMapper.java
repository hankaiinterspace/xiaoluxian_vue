package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.system.model.entity.SystemRole;
import cn.xuexiluxian.open.system.model.req.SystemRoleREQ;

import java.util.List;

/**
 * SystemRoleMapper继承基类
 */
public interface SystemRoleMapper extends BaseMapper<SystemRole> {
    List<SystemRole> selectListByUserId(String id);

    Page<SystemRole> page(Page page, SystemRoleREQ params);
}