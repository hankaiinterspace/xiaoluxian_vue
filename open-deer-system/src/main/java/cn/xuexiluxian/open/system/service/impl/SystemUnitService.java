package cn.xuexiluxian.open.system.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.common.utils.SecurityUtil;
import cn.xuexiluxian.open.system.mapper.SystemUnitMapper;
import cn.xuexiluxian.open.system.model.dto.SystemUnitDTO;
import cn.xuexiluxian.open.system.model.entity.SystemUnit;
import cn.xuexiluxian.open.system.service.ISystemUnitService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class SystemUnitService extends ServiceImpl<SystemUnitMapper, SystemUnit> implements ISystemUnitService {
    @Autowired
    private SystemUnitMapper systemUnitMapper;

    /**
     * 添加机构
     * @param systemUnitDTO
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean add(SystemUnitDTO systemUnitDTO) {
        String userId = SecurityUtil.getUserId();
        SystemUnit unit = new SystemUnit();
        BeanUtils.copyProperties(systemUnitDTO, unit);
        /**
         * 处理层级编码
         */
        String code = IdUtil.nanoId(6).toUpperCase();
        if(StrUtil.isNotBlank(systemUnitDTO.getParentId()) && !"-1".equals(systemUnitDTO.getParentId())){
            SystemUnit parentUnit = systemUnitMapper.selectById(systemUnitDTO.getParentId());
            unit.setCodeseq(parentUnit.getCodeseq() + "." + code);

            parentUnit.setHasChildren(1);
            systemUnitMapper.updateById(parentUnit);
        }else{
            unit.setCodeseq(code);
        }

        unit.setId(IdUtil.getSnowflakeNextIdStr());
        unit.setCode(code);
        unit.setHasChildren(0);
        unit.setSystem(0);
        if(systemUnitDTO.getEnabled() == null){
            unit.setEnabled(1);
        }
        unit.setCreateBy(userId);
        unit.setCreateTime(new Date());
        int row = systemUnitMapper.insert(unit);
        if(row > 0){
            return true;
        }
        return false;
    }

    /**
     * 删除机构
     * @param systemUnit
     * @return
     */
    @Override
    public boolean delete(SystemUnit systemUnit) {
        int row = systemUnitMapper.deleteById(systemUnit.getId());

        /**
         * 处理父级机构的HasChildren属性
         */
        List<SystemUnit> systemUnits = systemUnitMapper.selectByParentId(systemUnit.getParentId());
        if(systemUnits == null || systemUnits.size() <= 0) {
            SystemUnit parentUnit = new SystemUnit();
            parentUnit.setId(systemUnit.getParentId());
            parentUnit.setHasChildren(0);
            systemUnitMapper.updateById(parentUnit);
        }

        if(row > 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean modify(SystemUnitDTO systemUnitDTO) {
        String userId = SecurityUtil.getUserId();

        SystemUnit unit = new SystemUnit();
        BeanUtils.copyProperties(systemUnitDTO, unit);

        SystemUnit sourceUnit = systemUnitMapper.selectById(systemUnitDTO.getId());
        /**
         * 处理层级编码
         */
        if(StrUtil.isNotBlank(systemUnitDTO.getParentId()) && !"-1".equals(systemUnitDTO.getParentId())){
            SystemUnit parentUnit = systemUnitMapper.selectById(systemUnitDTO.getParentId());
            unit.setCodeseq(parentUnit.getCodeseq() + "." + sourceUnit.getCode());

            parentUnit.setHasChildren(1);
            systemUnitMapper.updateById(parentUnit);
        }else{
            unit.setCodeseq(sourceUnit.getCode());
        }


        unit.setCode(sourceUnit.getCode());
        unit.setHasChildren(sourceUnit.getHasChildren());
        unit.setSystem(0);
        if(systemUnitDTO.getEnabled() == null){
            unit.setEnabled(sourceUnit.getEnabled());
        }
        unit.setUpdateBy(userId);
        unit.setUpdateTime(new Date());
        int row = systemUnitMapper.updateById(unit);
        if(row > 0){
            return true;
        }
        return false;
    }
}
