package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.dto.SystemRoleDTO;
import cn.xuexiluxian.open.system.model.entity.SystemRole;
import cn.xuexiluxian.open.system.model.req.SystemRoleREQ;
import cn.xuexiluxian.open.system.model.vo.SystemRoleVO;

import java.util.Collection;
import java.util.List;

public interface ISystemRoleService extends IService<SystemRole> {
    Page<SystemRole> pageList(Page page, SystemRoleREQ params);

    boolean saveBean(SystemRoleDTO systemRoleDTO);

    boolean updateBean(SystemRoleDTO systemRoleDTO);

    List<SystemRole> selectListByUserId(String id);
}
