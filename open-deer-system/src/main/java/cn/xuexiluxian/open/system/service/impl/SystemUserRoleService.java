package cn.xuexiluxian.open.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.system.mapper.SystemUserRoleMapper;
import cn.xuexiluxian.open.system.model.entity.SystemUser;
import cn.xuexiluxian.open.system.model.entity.SystemUserRole;
import cn.xuexiluxian.open.system.model.req.SystemUserRoleREQ;
import cn.xuexiluxian.open.system.service.ISystemUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SystemUserRoleService extends ServiceImpl<SystemUserRoleMapper, SystemUserRole> implements ISystemUserRoleService {
    @Autowired
    private SystemUserRoleMapper systemUserRoleMapper;

    @Override
    public Page<SystemUser> selectListByRoleId(Page page, SystemUserRoleREQ params) {
        return systemUserRoleMapper.page(page, params);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean cancel(List<SystemUserRole> systemUserRoles) {
        int row = 0;
        for (int i = 0; i < systemUserRoles.size(); i++) {
            QueryWrapper<SystemUserRole> queryWrapper = new QueryWrapper();
            queryWrapper.eq("role_id", systemUserRoles.get(i).getRoleId());
            queryWrapper.eq("user_id", systemUserRoles.get(i).getUserId());
            row += systemUserRoleMapper.delete(queryWrapper);
        }
        if(row > 0){
            return true;
        }
        return false;
    }

    @Override
    public List<String> selectCheckedByUserId(String userId) {
        return systemUserRoleMapper.selectCheckedByUserId(userId);
    }
}
