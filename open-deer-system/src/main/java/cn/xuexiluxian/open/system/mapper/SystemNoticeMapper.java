package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.system.model.entity.SystemNotice;
import cn.xuexiluxian.open.system.model.req.SystemNoticeREQ;

/**
 * SystemNoticeMapper继承基类
 */
public interface SystemNoticeMapper extends BaseMapper<SystemNotice> {
    Page<SystemNotice> page(Page page, SystemNoticeREQ params);
}