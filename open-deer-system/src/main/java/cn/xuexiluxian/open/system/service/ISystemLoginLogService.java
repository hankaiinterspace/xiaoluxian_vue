package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.entity.SystemLoginLog;
import cn.xuexiluxian.open.system.model.req.LoginLogREQ;

/**
 * @Description
 * @Author 王俊南
 **/
public interface ISystemLoginLogService extends IService<SystemLoginLog> {
    Page pageList(Page page, LoginLogREQ bean);

    boolean clean();
}
