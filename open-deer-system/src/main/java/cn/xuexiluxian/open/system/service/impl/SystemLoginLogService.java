package cn.xuexiluxian.open.system.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.system.mapper.SystemLoginLogMapper;
import cn.xuexiluxian.open.system.model.entity.SystemLoginLog;
import cn.xuexiluxian.open.system.model.req.LoginLogREQ;
import cn.xuexiluxian.open.system.service.ISystemLoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Author 王俊南
 **/
@Service
public class SystemLoginLogService extends ServiceImpl<SystemLoginLogMapper, SystemLoginLog> implements ISystemLoginLogService {
    @Autowired
    private SystemLoginLogMapper sysLoginLogMapper;

    @Override
    public Page pageList(Page page, LoginLogREQ bean) {
        return sysLoginLogMapper.page(page, bean);
    }

    @Override
    public boolean clean() {
        sysLoginLogMapper.clean();
        return true;
    }
}
