package cn.xuexiluxian.open.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.system.mapper.SystemConfigMapper;
import cn.xuexiluxian.open.system.model.entity.SystemConfig;
import cn.xuexiluxian.open.system.model.req.SystemConfigREQ;
import cn.xuexiluxian.open.system.service.ISystemConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemConfigService extends ServiceImpl<SystemConfigMapper, SystemConfig> implements ISystemConfigService {
    @Autowired
    private SystemConfigMapper systemConfigMapper;
    @Override
    public Page<SystemConfig> pageList(Page page, SystemConfigREQ params) {
        return systemConfigMapper.page(page, params);
    }

    @Override
    public SystemConfig selectOneByKey(String key) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("k", key);
        queryWrapper.eq("enabled", 1);
        return systemConfigMapper.selectOne(queryWrapper);
    }
}
