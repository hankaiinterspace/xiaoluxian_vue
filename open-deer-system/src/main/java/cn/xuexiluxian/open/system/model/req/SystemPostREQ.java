package cn.xuexiluxian.open.system.model.req;

import lombok.Data;

@Data
public class SystemPostREQ {
    private String postName;
    private String postCode;
    private Integer enabled;
}
