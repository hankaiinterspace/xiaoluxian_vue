package cn.xuexiluxian.open.system.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Description 修改头像参数接收
 * @Author 王俊南
 * @Date 2022/6/4 11:15 上午
 **/
@Data
public class UpdateAvatarDTO {
    private String avatar;
    private String id;
}
