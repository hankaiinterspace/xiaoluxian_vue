package cn.xuexiluxian.open.system.model.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 
 * 
 */
@Data
@TableName(value = "system_dict_type")
public class SystemDictType implements Serializable {
    @TableId(type = IdType.AUTO)
    private String id;

    /**
     * 字典类型名称
     */
    private String name;

    /**
     * 分类
     */
    private String type;

    /**
     * 描述
     */
    private String remarks;

    private String createBy;

    private Date createTime;

    private String updateBy;

    private Date updateTime;

    private static final long serialVersionUID = 1L;
}