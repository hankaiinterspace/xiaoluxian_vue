package cn.xuexiluxian.open.system.model.req;

import lombok.Data;

@Data
public class SystemConfigREQ {
    /**
     * 键
     */
    private String key;

    /**
     * 是否启用（0：禁用；1：启用）
     */
    private Integer enabled;
}
