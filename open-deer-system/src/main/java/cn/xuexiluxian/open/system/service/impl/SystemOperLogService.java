package cn.xuexiluxian.open.system.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.system.mapper.SystemOperLogMapper;
import cn.xuexiluxian.open.system.model.entity.SystemOperLog;
import cn.xuexiluxian.open.system.model.req.OperateLogREQ;
import cn.xuexiluxian.open.system.service.ISystemOperLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Author 王俊南
 **/
@Service
public class SystemOperLogService extends ServiceImpl<SystemOperLogMapper, SystemOperLog> implements ISystemOperLogService {
    @Autowired
    private SystemOperLogMapper sysOperLogMapper;

    @Override
    public Page pageList(Page page, OperateLogREQ bean) {
        return sysOperLogMapper.page(page, bean);
    }

    @Override
    public boolean clean() {
        sysOperLogMapper.clean();
        return true;
    }
}
