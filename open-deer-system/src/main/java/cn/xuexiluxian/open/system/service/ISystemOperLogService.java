package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.entity.SystemOperLog;
import cn.xuexiluxian.open.system.model.req.OperateLogREQ;

/**
 * @Description
 * @Author 王俊南
 **/
public interface ISystemOperLogService extends IService<SystemOperLog> {
    Page pageList(Page page, OperateLogREQ bean);

    boolean clean();
}
