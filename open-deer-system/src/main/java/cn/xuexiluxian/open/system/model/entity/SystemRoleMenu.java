package cn.xuexiluxian.open.system.model.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 
 * 
 */
@Data
@TableName(value = "system_role_menu")
public class SystemRoleMenu implements Serializable {
    /**
     * 角色ID
     */
    private String roleId;

    /**
     * 菜单ID
     */
    private String menuId;

    private static final long serialVersionUID = 1L;
}