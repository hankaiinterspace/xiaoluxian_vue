package cn.xuexiluxian.open.system.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description
 * @Author 王俊南
 * @Date 2022/5/30 2:31 下午
 **/
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class SystemRoleVO implements Serializable {
    private String id;
    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色编码
     */
    private String rolePerm;
}
