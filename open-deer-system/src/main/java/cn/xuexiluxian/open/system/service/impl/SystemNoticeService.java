package cn.xuexiluxian.open.system.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.system.mapper.SystemNoticeMapper;
import cn.xuexiluxian.open.system.model.entity.SystemNotice;
import cn.xuexiluxian.open.system.model.req.SystemNoticeREQ;
import cn.xuexiluxian.open.system.service.ISystemNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemNoticeService extends ServiceImpl<SystemNoticeMapper, SystemNotice> implements ISystemNoticeService {
    @Autowired
    private SystemNoticeMapper systemNoticeMapper;

    @Override
    public Page<SystemNotice> pageList(Page page, SystemNoticeREQ params) {
        return systemNoticeMapper.page(page, params);
    }
}
