package cn.xuexiluxian.open.system.service.impl;

import cn.xuexiluxian.open.system.mapper.SystemUserPostMapper;
import cn.xuexiluxian.open.system.model.entity.SystemUserPost;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.system.mapper.SystemPostMapper;
import cn.xuexiluxian.open.system.model.entity.SystemPost;
import cn.xuexiluxian.open.system.model.req.SystemPostREQ;
import cn.xuexiluxian.open.system.service.ISystemPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SystemPostService extends ServiceImpl<SystemPostMapper, SystemPost> implements ISystemPostService {
    @Autowired
    private SystemPostMapper systemPostMapper;
    @Autowired
    private SystemUserPostMapper systemUserPostMapper;

    @Override
    public Page<SystemPost> pageList(Page page, SystemPostREQ params) {
        return systemPostMapper.page(page, params);
    }
}
