package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.entity.SystemMenu;
import cn.xuexiluxian.open.system.model.req.SystemMenuREQ;
import cn.xuexiluxian.open.system.model.vo.SystemRouterVO;

import java.util.Collection;
import java.util.List;

public interface ISystemMenuService extends IService<SystemMenu> {
    Page<SystemMenu> pageList(Page page, SystemMenuREQ params);

    List<SystemMenu> selectListByUserId(String id);

    List<SystemRouterVO> getRouters(String rolePerm);
}
