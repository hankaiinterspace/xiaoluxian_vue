package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.system.model.entity.SystemConfig;
import cn.xuexiluxian.open.system.model.req.SystemConfigREQ;

/**
 * SystemConfigMapper继承基类
 */
public interface SystemConfigMapper extends BaseMapper<SystemConfig> {
    Page<SystemConfig> page(Page page, SystemConfigREQ params);
}