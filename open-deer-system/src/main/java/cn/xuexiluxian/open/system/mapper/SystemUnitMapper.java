package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xuexiluxian.open.system.model.entity.SystemUnit;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * SystemUnitMapper继承基类
 */
public interface SystemUnitMapper extends BaseMapper<SystemUnit> {
    @Select("select * from system_unit where parent_id = #{parentId}")
    List<SystemUnit> selectByParentId(String parentId);
}