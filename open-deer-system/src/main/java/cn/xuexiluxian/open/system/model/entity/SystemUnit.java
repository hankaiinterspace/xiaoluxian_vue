package cn.xuexiluxian.open.system.model.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 
 * 
 */
@Data
@TableName(value = "system_unit")
public class SystemUnit implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private String id;

    /**
     * 机构名称
     */
    private String name;

    /**
     * 机构编码
     */
    private String code;

    /**
     * 机构层级编码
     */
    private String codeseq;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 网址
     */
    private String web;

    /**
     * 上级机构ID
     */
    private String parentId;

    /**
     * 是否有下级机构
     */
    private Integer hasChildren;

    /**
     * 是否系统内置
     */
    private Integer system;

    /**
     * 是否启用（0：禁用；1：启用）
     */
    private Integer enabled;

    /**
     * 负责人ID
     */
    private String leaderId;

    private String createBy;

    private Date createTime;

    private String updateBy;

    private Date updateTime;

    /**
     * 地址
     */
    private String address;

    private static final long serialVersionUID = 1L;
}