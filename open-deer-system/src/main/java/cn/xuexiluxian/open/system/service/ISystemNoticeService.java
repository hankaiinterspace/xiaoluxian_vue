package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.entity.SystemNotice;
import cn.xuexiluxian.open.system.model.req.SystemNoticeREQ;

public interface ISystemNoticeService extends IService<SystemNotice> {
    Page<SystemNotice> pageList(Page page, SystemNoticeREQ params);
}
