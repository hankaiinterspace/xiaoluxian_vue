package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.system.model.entity.SystemUser;
import cn.xuexiluxian.open.system.model.entity.SystemUserRole;
import cn.xuexiluxian.open.system.model.req.SystemUserRoleREQ;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * SystemUserRoleMapper继承基类
 */
public interface SystemUserRoleMapper extends BaseMapper<SystemUserRole> {
    Page<SystemUser> page(Page page, SystemUserRoleREQ params);

    @Select("select role_id from system_user_role where user_id = #{userId}")
    List<String> selectCheckedByUserId(String userId);
}