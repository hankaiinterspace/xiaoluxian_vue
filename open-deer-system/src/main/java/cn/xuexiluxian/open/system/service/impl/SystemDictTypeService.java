package cn.xuexiluxian.open.system.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.system.mapper.SystemDictTypeMapper;
import cn.xuexiluxian.open.system.model.entity.SystemDictType;
import cn.xuexiluxian.open.system.model.req.SystemDictTypeREQ;
import cn.xuexiluxian.open.system.service.ISystemDictTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SystemDictTypeService extends ServiceImpl<SystemDictTypeMapper, SystemDictType> implements ISystemDictTypeService {
    @Autowired
    private SystemDictTypeMapper systemDictTypeMapper;

    @Override
    public Page<SystemDictType> pageList(Page page, SystemDictTypeREQ params) {
        return systemDictTypeMapper.page(page, params);
    }
}
