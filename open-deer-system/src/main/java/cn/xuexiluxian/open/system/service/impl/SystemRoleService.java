package cn.xuexiluxian.open.system.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.common.utils.SecurityUtil;
import cn.xuexiluxian.open.system.mapper.SystemRoleMapper;
import cn.xuexiluxian.open.system.mapper.SystemRoleMenuMapper;
import cn.xuexiluxian.open.system.model.dto.SystemMenuDTO;
import cn.xuexiluxian.open.system.model.dto.SystemRoleDTO;
import cn.xuexiluxian.open.system.model.entity.SystemRole;
import cn.xuexiluxian.open.system.model.entity.SystemRoleMenu;
import cn.xuexiluxian.open.system.model.req.SystemRoleREQ;
import cn.xuexiluxian.open.system.model.vo.SystemRoleVO;
import cn.xuexiluxian.open.system.service.ISystemRoleMenuService;
import cn.xuexiluxian.open.system.service.ISystemRoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class SystemRoleService extends ServiceImpl<SystemRoleMapper, SystemRole> implements ISystemRoleService {
    @Autowired
    private SystemRoleMapper systemRoleMapper;
    @Autowired
    private ISystemRoleMenuService systemRoleMenuService;

    @Override
    public Page<SystemRole> pageList(Page page, SystemRoleREQ params) {
        return systemRoleMapper.page(page, params);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveBean(SystemRoleDTO systemRoleDTO) {
        String userId = SecurityUtil.getUserId();
        String unitId = SecurityUtil.getUnitId();
        String roleId = IdUtil.getSnowflakeNextIdStr();

        /**
         * 处理权限
         */
        List<String> permissionIds = systemRoleDTO.getPermissionIds();
        if(permissionIds != null && permissionIds.size() > 0){
            List<SystemRoleMenu> systemRoleMenus = new ArrayList<>();
            for (int i = 0;i < permissionIds.size();i++){
                SystemRoleMenu systemRoleMenu = new SystemRoleMenu();
                systemRoleMenu.setRoleId(roleId);
                systemRoleMenu.setMenuId(permissionIds.get(i));
                systemRoleMenus.add(systemRoleMenu);
            }
            systemRoleMenuService.saveBatch(systemRoleMenus);
        }

        SystemRole systemRole = new SystemRole();
        BeanUtils.copyProperties(systemRoleDTO, systemRole);
        systemRole.setId(roleId);
        systemRole.setUnitId(unitId);
        systemRole.setCreateBy(userId);
        systemRole.setCreateTime(new Date());

        int row = systemRoleMapper.insert(systemRole);
        if(row > 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean updateBean(SystemRoleDTO systemRoleDTO) {
        String userId = SecurityUtil.getUserId();
        String roleId = systemRoleDTO.getId();

        /**
         * 处理权限
         */
        QueryWrapper<SystemRoleMenu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id", roleId);
        systemRoleMenuService.remove(queryWrapper);
        List<String> permissionIds = systemRoleDTO.getPermissionIds();
        if(permissionIds != null && permissionIds.size() > 0){
            List<SystemRoleMenu> systemRoleMenus = new ArrayList<>();
            for (int i = 0;i < permissionIds.size();i++){
                SystemRoleMenu systemRoleMenu = new SystemRoleMenu();
                systemRoleMenu.setRoleId(roleId);
                systemRoleMenu.setMenuId(permissionIds.get(i));
                systemRoleMenus.add(systemRoleMenu);
            }
            systemRoleMenuService.saveBatch(systemRoleMenus);
        }

        SystemRole systemRole = new SystemRole();
        BeanUtils.copyProperties(systemRoleDTO, systemRole);
        systemRole.setUpdateBy(userId);
        systemRole.setUpdateTime(new Date());
        int row = systemRoleMapper.updateById(systemRole);
        if(row > 0){
            return true;
        }
        return false;
    }

    @Override
    public List<SystemRole> selectListByUserId(String id) {
        return systemRoleMapper.selectListByUserId(id);
    }
}
