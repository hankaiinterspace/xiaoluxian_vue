package cn.xuexiluxian.open.system.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Description 修改密码参数接收
 * @Author 王俊南
 * @Date 2022/6/4 11:15 上午
 **/
@Data
public class UpdatePasswordDTO {
    @NotBlank(message = "旧密码不能为空")
    private String oldPassword;
    @NotBlank(message = "新密码不能为空")
    private String newPassword;
}
