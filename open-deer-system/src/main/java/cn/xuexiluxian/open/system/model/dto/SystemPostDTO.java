package cn.xuexiluxian.open.system.model.dto;

import lombok.Data;

@Data
public class SystemPostDTO {
    private String id;
    private String postName;
    private String postCode;
    private Integer sort;
    private Integer enabled;
    private String remark;
}
