package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.system.model.entity.SystemUser;
import cn.xuexiluxian.open.system.model.req.SystemUserREQ;
import org.apache.ibatis.annotations.Select;

/**
 * SystemUserMapper继承基类
 */
public interface SystemUserMapper extends BaseMapper<SystemUser> {
    @Select("select * from system_user where phone = #{mobile}")
    SystemUser selectOneByMobile(String mobile);

    @Select("select * from system_user where username = #{username}")
    SystemUser selectOneByUserName(String username);

    Page<SystemUser> page(Page page, SystemUserREQ params);
}