package cn.xuexiluxian.open.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.common.constant.Constants;
import cn.xuexiluxian.open.common.utils.SecurityUtil;
import cn.xuexiluxian.open.common.utils.StringUtils;
import cn.xuexiluxian.open.system.mapper.SystemMenuMapper;
import cn.xuexiluxian.open.system.model.entity.SystemMenu;
import cn.xuexiluxian.open.system.model.req.SystemMenuREQ;
import cn.xuexiluxian.open.system.model.vo.SystemRouterMetaVo;
import cn.xuexiluxian.open.system.model.vo.SystemRouterVO;
import cn.xuexiluxian.open.system.service.ISystemMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Service
public class SystemMenuService extends ServiceImpl<SystemMenuMapper, SystemMenu> implements ISystemMenuService {
    @Autowired
    private SystemMenuMapper systemMenuMapper;

    @Override
    public Page<SystemMenu> pageList(Page page, SystemMenuREQ params) {
        return systemMenuMapper.page(page, params);
    }

    @Override
    public List<SystemMenu> selectListByUserId(String id) {
        return systemMenuMapper.selectListByUserId(id);
    }

    @Override
    public List<SystemRouterVO> getRouters(String rolePerm) {
        List<SystemMenu> systemMenus = new ArrayList<>();
        if(Constants.SUPER_ADMIN_ROLE.equals(rolePerm)){
            QueryWrapper<SystemMenu> queryWrapper = new QueryWrapper<SystemMenu>();
            queryWrapper.orderByAsc("sort");
            systemMenus = systemMenuMapper.selectList(queryWrapper);
        }else{
            systemMenus = systemMenuMapper.selectListByRolePerm(rolePerm);
        }
        List<SystemMenu> childPerms = getChildPerms(systemMenus, "-1");

        List<SystemRouterVO> routers = buildRouters(childPerms);
        return routers;
    }

    private List<SystemRouterVO> buildRouters(List<SystemMenu> systemMenus){
        List<SystemRouterVO> routers = new LinkedList<SystemRouterVO>();
        for (SystemMenu systemMenu: systemMenus) {
            SystemRouterVO systemRouterVO = new SystemRouterVO();
            systemRouterVO.setId(systemMenu.getId());
            systemRouterVO.setName(systemMenu.getName());
            systemRouterVO.setPath(systemMenu.getPath());
            systemRouterVO.setHidden(systemMenu.getVisible() == 1);
            systemRouterVO.setComponent(getComponent(systemMenu));
            systemRouterVO.setQuery(systemMenu.getQuery());
            systemRouterVO.setType(systemMenu.getType());
            systemRouterVO.setRedirect(systemMenu.getRedirect());
            systemRouterVO.setMeta(new SystemRouterMetaVo(systemMenu.getName(), systemMenu.getIcon(), systemMenu.getCache() == 1, systemMenu.getPath()));
            List<SystemMenu> cMenus = systemMenu.getChildren();
            if (!cMenus.isEmpty() && Constants.TYPE_DIR.equals(systemMenu.getType())) {
                systemRouterVO.setAlwaysShow(true);
                systemRouterVO.setChildren(buildRouters(cMenus));
            } else if ("-1".equals(systemMenu.getParentId()) && !isInnerLink(systemMenu)) {
                systemRouterVO.setMeta(new SystemRouterMetaVo(systemMenu.getName(), systemMenu.getIcon()));
                systemRouterVO.setPath("/");
                List<SystemRouterVO> childrenList = new ArrayList<SystemRouterVO>();
                SystemRouterVO children = new SystemRouterVO();
                children.setPath(systemMenu.getPath());
                children.setComponent(Constants.INNER_LINK);
                children.setName(StringUtils.capitalize(systemMenu.getPath()));
                children.setMeta(new SystemRouterMetaVo(systemMenu.getName(), systemMenu.getIcon(), systemMenu.getPath()));
                childrenList.add(children);
                systemRouterVO.setChildren(childrenList);
            }
            routers.add(systemRouterVO);
        }
        return routers;
    }

    /**
     * 获取组件信息
     *
     * @param menu 菜单信息
     * @return 组件信息
     */
    public String getComponent(SystemMenu menu) {
        String component = Constants.LAYOUT;
        if (StringUtils.isNotEmpty(menu.getComponent()) && !isMenuFrame(menu)) {
            component = menu.getComponent();
        } else if (StringUtils.isEmpty(menu.getComponent()) && "-1".equals(menu.getParentId()) && isInnerLink(menu)) {
            component = Constants.INNER_LINK;
        } else if (StringUtils.isEmpty(menu.getComponent()) && isParentView(menu)) {
            component = Constants.PARENT_VIEW;
        }
        return component;
    }

    /**
     * 是否为菜单内部跳转
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isMenuFrame(SystemMenu menu) {
        return "-1".equals(menu.getParentId()) && Constants.TYPE_MENU.equals(menu.getType()) && !isInnerLink(menu);
    }

    /**
     * 是否为内链组件
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isInnerLink(SystemMenu menu) {
        return StringUtils.ishttp(menu.getPath());
    }

    /**
     * 是否为parent_view组件
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isParentView(SystemMenu menu) {
        return "-1".equals(menu.getParentId()) && Constants.TYPE_DIR.equals(menu.getType());
    }

    /**
     * 根据父节点的ID获取所有子节点
     *
     * @param list     分类表
     * @param parentId 传入的父节点ID
     * @return String
     */
    public List<SystemMenu> getChildPerms(List<SystemMenu> list, String parentId) {
        List<SystemMenu> returnList = new ArrayList<SystemMenu>();
        for (Iterator<SystemMenu> iterator = list.iterator(); iterator.hasNext(); ) {
            SystemMenu t = iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId().equals(parentId)) {
                recursionFn(list, t);
                returnList.add(t);
            }
        }
        return returnList;
    }

    /**
     * 递归列表
     *
     * @param list
     * @param t
     */
    private void recursionFn(List<SystemMenu> list, SystemMenu t) {
        // 得到子节点列表
        List<SystemMenu> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SystemMenu tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SystemMenu> getChildList(List<SystemMenu> list, SystemMenu t) {
        List<SystemMenu> tlist = new ArrayList<SystemMenu>();
        Iterator<SystemMenu> it = list.iterator();
        while (it.hasNext()) {
            SystemMenu n = it.next();
            if (n.getParentId().equals(t.getId())) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SystemMenu> list, SystemMenu t) {
        return CollUtil.isNotEmpty(getChildList(list, t));
    }
}
