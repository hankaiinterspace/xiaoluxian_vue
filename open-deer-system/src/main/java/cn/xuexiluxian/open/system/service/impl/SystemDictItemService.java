package cn.xuexiluxian.open.system.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.system.mapper.SystemDictItemMapper;
import cn.xuexiluxian.open.system.model.entity.SystemDictItem;
import cn.xuexiluxian.open.system.model.req.SystemDictItemREQ;
import cn.xuexiluxian.open.system.service.ISystemDictItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemDictItemService extends ServiceImpl<SystemDictItemMapper, SystemDictItem> implements ISystemDictItemService {
    @Autowired
    private SystemDictItemMapper systemDictItemMapper;

    @Override
    public Page<SystemDictItem> pageList(Page page, SystemDictItemREQ params) {
        return systemDictItemMapper.page(page, params);
    }

    @Override
    public List<SystemDictItem> selectListByDictType(String dictType) {
        return systemDictItemMapper.selectListByDictType(dictType);
    }
}
