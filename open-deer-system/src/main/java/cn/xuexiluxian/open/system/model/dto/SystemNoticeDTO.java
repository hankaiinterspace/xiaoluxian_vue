package cn.xuexiluxian.open.system.model.dto;

import lombok.Data;

@Data
public class SystemNoticeDTO {
    private String id;
    private String title;
    private Integer type;
    private Integer enabled;
    private String content;
}
