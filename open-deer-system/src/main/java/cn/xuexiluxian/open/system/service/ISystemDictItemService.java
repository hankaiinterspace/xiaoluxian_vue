package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.entity.SystemDictItem;
import cn.xuexiluxian.open.system.model.req.SystemDictItemREQ;

import java.util.List;

public interface ISystemDictItemService extends IService<SystemDictItem> {
    Page<SystemDictItem> pageList(Page page, SystemDictItemREQ params);

    List<SystemDictItem> selectListByDictType(String dictType);
}
