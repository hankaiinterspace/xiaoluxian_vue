package cn.xuexiluxian.open.system.model.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 
 * 
 */
@Data
@TableName(value = "system_notice")
public class SystemNotice implements Serializable {
    /**
     * 公告ID
     */
    @TableId(type = IdType.AUTO)
    private String id;

    /**
     * 标题
     */
    private String title;

    /**
     * 类型（1：通知；2：公告）
     */
    private Integer type;

    /**
     * 状态（0：禁用；1：启用）
     */
    private Integer enabled;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 内容
     */
    private String content;

    private static final long serialVersionUID = 1L;
}