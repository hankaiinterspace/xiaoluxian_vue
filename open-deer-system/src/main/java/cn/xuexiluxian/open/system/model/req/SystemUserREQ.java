package cn.xuexiluxian.open.system.model.req;

import lombok.Data;

@Data
public class SystemUserREQ {
    /**
     * 登录账号
     */
    private String username;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户性别（1：男；2：女；0：未知）
     */
    private Integer gender;

    /**
     * 帐号状态（0：禁用；1：正常）
     */
    private Integer enabled;

    /**
     * 机构ID
     */
    private String unitId;
}
