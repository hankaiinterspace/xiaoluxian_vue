package cn.xuexiluxian.open.system.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 
 * 
 */
@Data
@TableName(value = "system_menu")
public class SystemMenu implements Serializable {
    /**
     * 菜单ID
     */
    @TableId(type = IdType.AUTO)
    private String id;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 父菜单ID
     */
    private String parentId;

    /**
     * 显示顺序
     */
    private Integer sort;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 路由参数
     */
    private String query;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 是否缓存（0：缓存；1：不缓存）
     */
    private Integer cache;

    /**
     * 菜单类型（0：目录；1：菜单；2：按钮）
     */
    private Integer type;

    /**
     * 显示状态（0：显示；1：隐藏）
     */
    private Integer visible;

    /**
     * 菜单状态（0：禁用；1：启用）
     */
    private Integer enabled;

    /**
     * 权限标识
     */
    private String perms;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 重写向
     */
    private String redirect;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    @TableField(exist = false)
    private List<SystemMenu> children = new ArrayList<>();

    private static final long serialVersionUID = 1L;
}