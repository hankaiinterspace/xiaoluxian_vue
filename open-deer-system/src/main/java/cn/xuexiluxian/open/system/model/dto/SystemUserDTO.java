package cn.xuexiluxian.open.system.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class SystemUserDTO {
    /**
     * 用户ID
     */
    private String id;

    /**
     * 登录账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 用户邮箱
     */
    private String email;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户性别（1：男；2：女；0：未知）
     */
    private Integer gender;

    /**
     * 头像路径
     */
    private String avatar;

    /**
     * 帐号状态（0：禁用；1：正常）
     */
    private Integer enabled;

    /**
     * 备注
     */
    private String remark;

    private List<String> roleIds;

    private List<String> unitIds;

    private List<String> postIds;
}
