package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.system.model.entity.SystemOperLog;
import cn.xuexiluxian.open.system.model.req.OperateLogREQ;
import org.apache.ibatis.annotations.Delete;

/**
 * @Description
 * @Author 王俊南
 **/
public interface SystemOperLogMapper extends BaseMapper<SystemOperLog> {
    @Delete("truncate table system_oper_log")
    void clean();

    Page page(Page page, OperateLogREQ params);
}
