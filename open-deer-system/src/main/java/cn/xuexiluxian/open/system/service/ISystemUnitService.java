package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.dto.SystemUnitDTO;
import cn.xuexiluxian.open.system.model.entity.SystemUnit;

public interface ISystemUnitService extends IService<SystemUnit> {
    boolean add(SystemUnitDTO systemUnitDTO);

    boolean delete(SystemUnit systemUnit);

    boolean modify(SystemUnitDTO systemUnitDTO);
}
