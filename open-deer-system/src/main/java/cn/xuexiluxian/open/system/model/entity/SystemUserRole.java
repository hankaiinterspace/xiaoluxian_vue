package cn.xuexiluxian.open.system.model.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 
 * 
 */
@Data
@TableName(value = "system_user_role")
public class SystemUserRole implements Serializable {
    private String roleId;

    private String userId;

    private static final long serialVersionUID = 1L;
}