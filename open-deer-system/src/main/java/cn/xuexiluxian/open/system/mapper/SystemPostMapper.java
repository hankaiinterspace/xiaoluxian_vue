package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.system.model.entity.SystemPost;
import cn.xuexiluxian.open.system.model.req.SystemPostREQ;

/**
 * SystemPostMapper继承基类
 */
public interface SystemPostMapper extends BaseMapper<SystemPost> {
    Page<SystemPost> page(Page page, SystemPostREQ params);
}