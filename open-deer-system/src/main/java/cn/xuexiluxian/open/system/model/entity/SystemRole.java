package cn.xuexiluxian.open.system.model.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 
 * 
 */
@Data
@TableName(value = "system_role")
public class SystemRole implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private String id;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色编码
     */
    private String rolePerm;

    /**
     * 机构ID
     */
    private String unitId;

    /**
     * 数据权限
     */
    private Integer dataPrivileges;

    /**
     * 是否启用（0：禁用；1：启用）
     */
    private Integer enabled;

    private String createBy;

    private Date createTime;

    private String updateBy;

    private Date updateTime;

    /**
     * 描述
     */
    private String descript;

    private static final long serialVersionUID = 1L;
}