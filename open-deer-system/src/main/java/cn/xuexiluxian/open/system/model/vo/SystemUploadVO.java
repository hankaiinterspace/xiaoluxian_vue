package cn.xuexiluxian.open.system.model.vo;

import lombok.Builder;
import lombok.Data;

/**
 * @Description
 * @Author 王俊南
 * @Date 2022/5/22 2:37 下午
 **/
@Data
@Builder
public class SystemUploadVO {
    /**
     * 文件访问路径
     */
    private String url;
    /**
     * 新生成的文件名
     */
    private String newFileName;
    /**
     * 原始的文件名
     */
    private String originalFileName;
    /**
     * 文件名后缀
     */
    private String suffix;
    /**
     * 文件大小KB
     */
    private Long size;
}
