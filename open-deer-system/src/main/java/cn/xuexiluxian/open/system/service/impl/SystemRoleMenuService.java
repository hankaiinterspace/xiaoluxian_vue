package cn.xuexiluxian.open.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xuexiluxian.open.system.mapper.SystemRoleMenuMapper;
import cn.xuexiluxian.open.system.model.entity.SystemRoleMenu;
import cn.xuexiluxian.open.system.service.ISystemRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemRoleMenuService extends ServiceImpl<SystemRoleMenuMapper, SystemRoleMenu> implements ISystemRoleMenuService {
    @Autowired
    private SystemRoleMenuMapper systemRoleMenuMapper;

    @Override
    public List<String> selectCheckedByRoleId(String id) {
        return systemRoleMenuMapper.selectCheckedByRoleId(id);
    }
}
