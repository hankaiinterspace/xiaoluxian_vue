package cn.xuexiluxian.open.system.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SystemUnitDTO implements Serializable {
    private String id;
    private String name;
    private String contact;
    private String mobile;
    private String email;
    private String web;
    private String parentId;
    private Integer enabled;
    private String leaderId;
    private String address;
}
