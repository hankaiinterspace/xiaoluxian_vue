package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.system.model.entity.SystemMenu;
import cn.xuexiluxian.open.system.model.req.SystemMenuREQ;

import java.util.List;
import java.util.Set;

/**
 * SystemMenuMapper继承基类
 */
public interface SystemMenuMapper extends BaseMapper<SystemMenu> {
    List<SystemMenu> selectListByUserId(String id);

    Page<SystemMenu> page(Page page, SystemMenuREQ params);

    List<SystemMenu> selectListByRolePerm(String rolePerm);
}