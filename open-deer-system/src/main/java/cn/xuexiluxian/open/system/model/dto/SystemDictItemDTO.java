package cn.xuexiluxian.open.system.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class SystemDictItemDTO {
    @TableId(type = IdType.AUTO)
    private String id;

    /**
     * 字典类型ID
     */
    private String typeId;

    /**
     * 键
     */
    private String k;

    /**
     * 值
     */
    private String v;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 描述
     */
    private String remark;
}
