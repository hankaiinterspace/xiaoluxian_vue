package cn.xuexiluxian.open.system.service;

import cn.xuexiluxian.open.system.model.entity.SystemUserRole;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.dto.SystemUserDTO;
import cn.xuexiluxian.open.system.model.entity.SystemUser;
import cn.xuexiluxian.open.system.model.req.SystemUserREQ;

import java.util.List;
import java.util.Map;

public interface ISystemUserService extends IService<SystemUser> {
    Page<SystemUser> pageList(Page page, SystemUserREQ params);

    boolean saveBean(SystemUserDTO systemUserDTO);

    boolean updateBean(SystemUserDTO systemUserDTO);

    SystemUser selectOneByUserName(String username);

    SystemUser selectOneByMobile(String phone);

    boolean delete(String id);

    Map<String, Object> detail(String id);

    boolean reset(String id);

    boolean grant(List<SystemUserRole> list);
}
