package cn.xuexiluxian.open.system.model.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 
 * 
 */
@Data
@TableName(value = "system_user_post")
public class SystemUserPost implements Serializable {
    private String userId;

    private String postId;

    private static final long serialVersionUID = 1L;
}