package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.entity.SystemUserPost;

import java.util.List;

public interface ISystemUserPostService extends IService<SystemUserPost> {
    List<String> selectCheckedByUserId(String userId);
}
