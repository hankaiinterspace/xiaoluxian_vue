package cn.xuexiluxian.open.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xuexiluxian.open.system.model.entity.SystemDictType;
import cn.xuexiluxian.open.system.model.req.SystemDictTypeREQ;

/**
 * SystemDictTypeMapper继承基类
 */
public interface SystemDictTypeMapper extends BaseMapper<SystemDictType> {
    Page<SystemDictType> page(Page page, SystemDictTypeREQ params);
}