package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.entity.SystemPost;
import cn.xuexiluxian.open.system.model.req.SystemPostREQ;

public interface ISystemPostService extends IService<SystemPost> {
    Page<SystemPost> pageList(Page page, SystemPostREQ params);
}
