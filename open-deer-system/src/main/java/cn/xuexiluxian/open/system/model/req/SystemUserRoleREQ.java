package cn.xuexiluxian.open.system.model.req;

import lombok.Data;

@Data
public class SystemUserRoleREQ {
    private String roleId;
    private String username;
    private String mobile;
}
