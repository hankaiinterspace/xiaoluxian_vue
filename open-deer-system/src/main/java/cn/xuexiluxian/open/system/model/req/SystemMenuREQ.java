package cn.xuexiluxian.open.system.model.req;

import lombok.Data;

@Data
public class SystemMenuREQ {
    /**
     * 菜单名称
     */
    private String name;

    /**
     * 父菜单ID
     */
    private String parentId;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 是否缓存（0：缓存；1：不缓存）
     */
    private Integer cache;

    /**
     * 菜单类型（0：目录；1：菜单；2：按钮）
     */
    private Integer type;

    /**
     * 显示状态（0：显示；1：隐藏）
     */
    private Integer visible;

    /**
     * 菜单状态（0：禁用；1：启用）
     */
    private Integer enabled;

    /**
     * 权限标识
     */
    private String perms;
}
