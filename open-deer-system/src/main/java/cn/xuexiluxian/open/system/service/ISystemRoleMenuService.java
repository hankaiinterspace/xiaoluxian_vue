package cn.xuexiluxian.open.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xuexiluxian.open.system.model.entity.SystemRoleMenu;

import java.util.List;

public interface ISystemRoleMenuService extends IService<SystemRoleMenu> {
    List<String> selectCheckedByRoleId(String id);
}
