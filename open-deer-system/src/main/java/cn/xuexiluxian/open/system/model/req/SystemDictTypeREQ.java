package cn.xuexiluxian.open.system.model.req;

import lombok.Data;

@Data
public class SystemDictTypeREQ {
    private String name;
    private String type;
}
