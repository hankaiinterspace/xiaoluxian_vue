package cn.xuexiluxian.open.framework.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * 手机验证码登录token
 */
public class MobileAuthenticationToken extends AbstractAuthenticationToken {

    // 认证之前放手机号，认证之后放用户信息
    private final Object principal;

    /**
     * 开始认证时,创建一个MobileAuthenticationToken实例 接收的是手机号码, 并且 标识未认证
     * @param principal 手机号
     */
    public MobileAuthenticationToken(Object principal) {
        super(null);
        this.principal = principal; // 手机号
        setAuthenticated(false);
    }

    /**
     * 当认证通过后,会重新创建一个新的MobileAuthenticationToken,来标识它已经认证通过,
     * @param principal 用户信息
     * @param authorities 用户权限
     */
    public MobileAuthenticationToken(Object principal, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = principal; // 用户信息
        super.setAuthenticated(true); // 标识已经认证通过
    }

    /**
     * 在父类中是一个抽象方法,所以要实现, 但是它是密码,而当前不需要,则直接返回null
     * @return
     */
    @Override
    public Object getCredentials() {
        return null;
    }

    /**
     * 手机号码
     * @return
     */
    @Override
    public Object getPrincipal() {
        return this.principal;
    }

    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException( "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        }
        super.setAuthenticated(false);
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
    }
}
