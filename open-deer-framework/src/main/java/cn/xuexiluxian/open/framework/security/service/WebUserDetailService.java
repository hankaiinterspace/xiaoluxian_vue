package cn.xuexiluxian.open.framework.security.service;

import cn.hutool.core.util.StrUtil;
import cn.xuexiluxian.open.common.entity.LoginUser;
import cn.xuexiluxian.open.system.mapper.SystemMenuMapper;
import cn.xuexiluxian.open.system.mapper.SystemRoleMapper;
import cn.xuexiluxian.open.system.mapper.SystemUnitMapper;
import cn.xuexiluxian.open.system.mapper.SystemUserMapper;
import cn.xuexiluxian.open.system.model.entity.SystemMenu;
import cn.xuexiluxian.open.system.model.entity.SystemRole;
import cn.xuexiluxian.open.system.model.entity.SystemUnit;
import cn.xuexiluxian.open.system.model.entity.SystemUser;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service("webUserDetailService")
public class WebUserDetailService implements UserDetailsService {
    @Autowired
    private SystemUserMapper systemUserMapper;
    @Autowired
    private SystemRoleMapper systemRoleMapper;
    @Autowired
    private SystemMenuMapper systemMenuMapper;
    @Autowired
    private SystemUnitMapper systemUnitMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (StrUtil.isBlank(username)) {
            throw new UsernameNotFoundException("用户名不能为空");
        }

        SystemUser user = systemUserMapper.selectOneByUserName(username);
        if(user == null){
            throw new UsernameNotFoundException("用户不存在");
        }
        if (user.getEnabled() != 1) {
            throw new DisabledException("用户已禁用");
        }

        Set<String> roles = new HashSet<>();
        Set<String> permissions = new HashSet<>();
        if (user.getUserType() == 1) {
            roles.add("SUPER_ADMIN");
            permissions.add("*:*:*");
        } else {
            // 查询角色
            List<SystemRole> systemRoles = systemRoleMapper.selectListByUserId(user.getId());
            roles = systemRoles.stream().map(SystemRole::getRolePerm).collect(Collectors.toSet());
            // 查询菜单、权限
            List<SystemMenu> systemMenus = systemMenuMapper.selectListByUserId(user.getId());
            permissions = systemMenus.stream().map(SystemMenu::getPerms).collect(Collectors.toSet());
        }
        // 机构
        SystemUnit systemUnits = systemUnitMapper.selectById(user.getUnitId());

        LoginUser loginUser = new LoginUser();
        BeanUtils.copyProperties(user,loginUser);
        loginUser.setRoles(roles);
        loginUser.setPermissions(permissions);
        loginUser.setUnit(systemUnits);

        return loginUser;
    }
}
