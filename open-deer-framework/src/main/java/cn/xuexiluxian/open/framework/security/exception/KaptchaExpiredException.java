package cn.xuexiluxian.open.framework.security.exception;

import org.springframework.security.core.AuthenticationException;

public class KaptchaExpiredException extends AuthenticationException {

    public KaptchaExpiredException(String msg) {
        super(msg);
    }

    public KaptchaExpiredException(String msg, Throwable t) {
        super(msg, t);
    }
}
