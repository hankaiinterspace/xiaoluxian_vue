package cn.xuexiluxian.open.framework.security.exception;

import org.springframework.security.core.AuthenticationException;

public class KaptchaValidateException extends AuthenticationException {
    public KaptchaValidateException(String msg) {
        super(msg);
    }

    public KaptchaValidateException(String msg, Throwable t) {
        super(msg, t);
    }
}
