package cn.xuexiluxian.open.framework.security.handler;

import cn.hutool.core.util.IdUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.alibaba.fastjson.JSON;
import cn.xuexiluxian.open.common.response.ResponseResult;
import cn.xuexiluxian.open.framework.event.SystemLoginLogEvent;
import cn.xuexiluxian.open.framework.security.utils.JwtTokenUtil;
import cn.xuexiluxian.open.system.model.entity.SystemLoginLog;
import lombok.extern.slf4j.Slf4j;
import net.dreamlu.mica.ip2region.core.Ip2regionSearcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Slf4j
@Component("customAuthenticationSuccessHandler")
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Autowired
    private Ip2regionSearcher regionSearcher;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        UserAgent userAgent = UserAgentUtil.parse(request.getHeader("User-Agent"));
        String ip = ServletUtil.getClientIP(request);
        String address = regionSearcher.getAddress(ip);
        // 获取客户端操作系统
        String os = userAgent.getOs().getName();
        // 获取客户端浏览器
        String browser = userAgent.getBrowser().getName();
        // 封装对象
        SystemLoginLog sysLoginLog = new SystemLoginLog();
        sysLoginLog.setId(IdUtil.getSnowflakeNextIdStr());
        sysLoginLog.setUserName(authentication.getName());
        sysLoginLog.setIpaddr(ip);
        sysLoginLog.setLoginLocation(address);
        sysLoginLog.setBrowser(browser);
        sysLoginLog.setOs(os);
        sysLoginLog.setMsg("登录");
        sysLoginLog.setLoginTime(new Date());
        SpringUtil.getApplicationContext().publishEvent(new SystemLoginLogEvent(sysLoginLog));

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails)auth.getPrincipal();
        String token = JwtTokenUtil.generateToken(userDetails);
        cn.xuexiluxian.open.common.utils.ServletUtil.renderString(response, JSON.toJSONString(ResponseResult.ok(token)));

    }
}
