package cn.xuexiluxian.open.framework.security.filter;

import cn.hutool.core.util.StrUtil;
import cn.xuexiluxian.open.common.constant.Constants;
import cn.xuexiluxian.open.common.enums.SmsCodeEnum;
import cn.xuexiluxian.open.common.utils.AesUtil;
import cn.xuexiluxian.open.framework.security.RequestWrapper;
import cn.xuexiluxian.open.framework.security.exception.KaptchaExpiredException;
import cn.xuexiluxian.open.framework.security.exception.KaptchaValidateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 手机验证码登录过滤器
 */
@Component
public class MobileCaptchaCodeValidateFilter extends OncePerRequestFilter {
    /**
     * 登录失败处理器
     */
    @Autowired
    private AuthenticationFailureHandler customAuthenticationFailureHandler;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        RequestWrapper requestWrapper = new RequestWrapper(request);
        if (Constants.MOBILE_LOGIN_URL.equals(request.getRequestURI()) && "post".equalsIgnoreCase(request.getMethod())) {
            try {
                validate(requestWrapper);
            } catch (AuthenticationException e) {
                customAuthenticationFailureHandler.onAuthenticationFailure(request, response, e);
                return;
            }
        }
        filterChain.doFilter(requestWrapper, response);
    }

    private void validate(HttpServletRequest request) {
        String mobile = request.getParameter("mobile");
        String inputKaptcha = request.getParameter("captcha");

        if (StrUtil.isBlank(mobile)) {
            throw new KaptchaValidateException("手机号不能为空");
        }

        if (StrUtil.isBlank(inputKaptcha)) {
            throw new KaptchaValidateException("短信验证码不能为空");
        }

        try {
            //解密
            mobile = AesUtil.decrypt(mobile);
        } catch (Exception ignored) {}
        try {
            //解密
            inputKaptcha = AesUtil.decrypt(inputKaptcha);
        } catch (Exception ignored) {}

        String kaptcha = redisTemplate.opsForValue().get(SmsCodeEnum.LOGIN.getKey() + mobile);
        if (StrUtil.isBlank(kaptcha)) {
            throw new KaptchaExpiredException("短信验证码已过期");
        }

        if (!kaptcha.equalsIgnoreCase(inputKaptcha)) {
            throw new KaptchaValidateException("短信验证码输入错误");
        }
    }
}
