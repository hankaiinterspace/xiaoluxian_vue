package cn.xuexiluxian.open.framework.event;

import cn.xuexiluxian.open.system.model.entity.SystemOperLog;
import org.springframework.context.ApplicationEvent;

/**
 * 操作日志事件
 */
public class SystemOperLogEvent extends ApplicationEvent {

	public SystemOperLogEvent(SystemOperLog source) {
		super(source);
	}

}
