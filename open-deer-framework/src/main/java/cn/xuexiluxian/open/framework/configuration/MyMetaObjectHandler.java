package cn.xuexiluxian.open.framework.configuration;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import cn.xuexiluxian.open.common.utils.SecurityUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Description mybatis plus字段自动填充
 * @Author 王俊南
 * @Date 2022/5/17 10:20 上午
 **/
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject, "createBy", () -> SecurityUtil.getUserId(), String.class);
        this.strictInsertFill(metaObject, "createTime", () -> new Date(), Date.class); // 起始版本 3.3.3(推荐)
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "updateBy", () -> SecurityUtil.getUserId(), String.class);
        this.strictUpdateFill(metaObject, "updateTime", () -> new Date(), Date.class); // 起始版本 3.3.3(推荐)
    }
}
