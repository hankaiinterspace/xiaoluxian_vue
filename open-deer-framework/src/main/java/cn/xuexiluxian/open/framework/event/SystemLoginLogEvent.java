package cn.xuexiluxian.open.framework.event;

import cn.xuexiluxian.open.system.model.entity.SystemLoginLog;
import org.springframework.context.ApplicationEvent;

/**
 * 登录日志事件
 */
public class SystemLoginLogEvent extends ApplicationEvent {

	public SystemLoginLogEvent(SystemLoginLog source) {
		super(source);
	}

}
