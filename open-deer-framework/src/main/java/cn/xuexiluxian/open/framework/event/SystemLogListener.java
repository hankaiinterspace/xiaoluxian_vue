package cn.xuexiluxian.open.framework.event;

import cn.xuexiluxian.open.system.model.entity.SystemLoginLog;
import cn.xuexiluxian.open.system.model.entity.SystemOperLog;
import cn.xuexiluxian.open.system.service.ISystemLoginLogService;
import cn.xuexiluxian.open.system.service.ISystemOperLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 *
 */
@Slf4j
@Component
public class SystemLogListener {
    @Autowired
    private ISystemOperLogService sysOperLogService;
    @Autowired
    private ISystemLoginLogService sysLoginLogService;

    @Async("threadPoolTaskExecutor")
    @EventListener(SystemOperLogEvent.class)
    public void handlerSysOperLog(SystemOperLogEvent event) {
        SystemOperLog sysOperLog = (SystemOperLog) event.getSource();
        sysOperLogService.save(sysOperLog);
    }

    @Async("threadPoolTaskExecutor")
    @EventListener(SystemLoginLogEvent.class)
    public void handlerSysLoginLog(SystemLoginLogEvent event) {
        SystemLoginLog sysLoginLog = (SystemLoginLog) event.getSource();
        sysLoginLogService.save(sysLoginLog);
    }


}
